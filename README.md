# Requirements / technologies #
* PHP 7 & MySQL 
* Angular JS
* Laravel

# Documentation #
https://docs.google.com/document/d/14BqIg5RVO6gj2O2KOzl2UMRvOE0iaX7-Uj1wGXWRCQc/edit?usp=sharing

# Jak pobrać Nordal na własny PC #
1. Sklonuj to repozytorium (np. wklejając link z prawego górnego rogu do Twojego programu do zarządzania Gitem np. Source Tree)
2. Upewnij się, że masz zainstalowany composer na komputerze (np. wpisując `composer` w konsoli).
3. Zainstaluj odpowiednie paczki projektu poprzez composer: Wejdź w konsoli do folderu `/nordal` i wpisz `composer install`
4. Utwórz nową bazę danych na swoim komputerze.
5. Otwórz plik `Nordal/.env.example` i wpisz odpowiednie wartości dla połączenia z bazą danych. Potem zapisz ten plik jako `.env` w tej samej lokalizacji, w której był plik `.enx.example`. Info: Plik `.env` jest plikiem środowiska (and. *environment*). Pliki ustawień w katalogu `config` pobierają wartości z tego pliku. Dla każdej instancji Nordalu (np. na serwerze, lokalnym komputerze) wystarczy podmienić plik `.env` i wszystko powinno działać.
6. Wejdź w konsoli do katalogu `Nordal` i wpisz (Windows:) `php artisan key:generate`
7. Wejdź w konsoli do katalogu `Nordal` i wpisz (Windows:) `php artisan migrate`, żeby stworzyć wszystkie tabele i zmiany w strukturze bazy danych zapisane w pliku `Nordal/database/migrations`.
8. W konsoli, w katalogu `Nordal` wpisz `npm install`. Jeżeli nie masz zainstalowane `node.js`, przejdź tutaj: https://laravel.com/docs/5.2/elixir#installation

### Aby zainstalować Nordal na Windowsie z IIS należy: ###
1. Dodać nazwę hosta do pliku hosts
2. Stworzyć nową witrynę w IIS i przypisać do niej Application Pool z .NET frameworkiem v4.0
3. Scieżka ma wskazywać na folder `/Nordal/public`
 4. Dodać Wirtualny folder `storage` i nadać mu pełne uprawnienia dla użytkownika `IUSER`
 
 Warto jeszcze sprawdzić link https://laracasts.com/discuss/channels/general-discussion/iis7-laravel-5