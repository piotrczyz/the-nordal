<?php

use Nordal\Models\Pin;
use Nordal\Models\Task;
use Nordal\Models\TaskType;

class TaskTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTaskExist()
    {
        $this->seeInDatabase('tasks',['id' => '3']);
    }
    
    public function testInsertTasks(){
    	
    	$task = factory(Task::class, 10)->create([
    		'TypeId' => function(){
    		    return factory(TaskType::class)->create()->Id;
		    },
		    'PinId' => function(){
			    return factory(Pin::class)->create()->Id;
		    },
	    ]);
	    
	    $this->assertEquals(count($task),10);	    
    }
    
    
}
