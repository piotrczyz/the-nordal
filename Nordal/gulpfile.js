var elixir = require('laravel-elixir');
elixir.config.publicDir = 'public';
elixir.config.publicPath = 'public';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass(['app.scss'], 'public/css/app.css');

    // Application Scripts
    mix.scripts([
        'lib/jquery.min.js',
        'lib/jquery-ui.min.js',
        'lib/jquery.ui.touch-punch.min.js',
        'lib/bootstrap.min.js',
        'other/helpers.js'
    ], 'public/js/app.js');

    // Application Scripts
    mix.scripts([
        'lib/angular.js',
        'lib/angular-ui-router.min.js',
        'lib/angular-animate.min.js',
        '../bower_components/angular-audio/app/angular.audio.js',
        '../bower_components/angular-scroll-glue/src/scrollglue.js',
        'app.js',
        'factories',
        'controllers',
        'directives'
    ], 'public/js/adventure.js');

    mix.version([
        'css/app.css',
        'js/app.js',
        'js/adventure.js'
    ]);
});
