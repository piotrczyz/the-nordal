<?php

Route::auth();
Route::get('/', 'HomeController@index');

Route::get('/adventure', ['as'=>'adventure', 'uses' => 'AdventureController@index']);

Route::get('user/profile','UserController@index');
Route::patch('user/{user}','UserController@update');

Route::get('map/get/{mapId?}', 'MapController@GetMap'); //TODO @piotr get->post
Route::get('task/get/{taskId}', 'TaskController@GetTask'); //TODO @piotr get->post
Route::post('task/complete/{dto?}', 'TaskController@CompleteTask'); //TODO @piotr get->post
Route::get('user/get', 'UserComtroller@GetUser'); //TODO @piotr get->post
Route::get('stats/gettask/{taskId}', 'StatsController@getTask');

Route::get('stats/task/{taskId}','StatsController@task');

Route::get('migrate', function() {
    $output = Artisan::call('migrate');
    dd($output);
});