<?php

namespace Nordal\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nordal\Definitions\ResponseCode;
use Nordal\Dtos\TaskCompleteDto;
use Nordal\Exceptions\OnCompletedTaskException;
use Nordal\Providers\TaskServiceProvider;
use Nordal\Results\JsonResult;
use Nordal\Definitions\TaskType;
use Nordal\Models\Task;

class TaskController extends Controller
{
	/**
	 * @var TaskServiceProvider
	 */
	private $provider;

	public function __construct(TaskServiceProvider $provider)
	{
		parent::__construct();

		$this->provider = $provider;
		$this->provider->SetUser(Auth::user());
	}
	
	public function GetTask($taskId){

		$result = new JsonResult();
		
		$dtoTask = $this->provider->GetTask($taskId);
		if ($dtoTask === null) {
			$result->Message = trans('app.NoRecordFound');
			$result->Status = ResponseCode::NOTFOUND;
			return response()->json($result);
		}
		
		$result->Status = ResponseCode::SUCCESS;
		$result->Result = $dtoTask;
		return response()->json($result);
	}
	
	public function CompleteTask(Request $request){

		$result      = new JsonResult();
		$dto         = new TaskCompleteDto(); //TODO idzie to zrobić automatycznie w routes?
		$dto->TaskId = (int)$request->input('TaskId');

		$dto->Data   = is_array($request->input('Data')) ? array_filter($request->input('Data')):array();
		
		if ($dto->TaskId){
			try {
			    // Dodałem tu !== false, bo czasem zadania dają 0 expa
			    $completeresult = $this->provider->CompleteTask($dto);
				if(($exp = $completeresult['exp']) !== false) {
                    $result->Status = ResponseCode::SUCCESS;
                    if(Task::find($dto->TaskId)->Type->Param1 == TaskType::SENTENCES) {
                    	if($completeresult['repeated']) {
							$result->Message = trans('app.CompletedRepeatedSentencesSuccess', ['exp' => $exp, 'fish-icon' => '<img src="img/misc/fish.svg" alt="rybek" class="fish" />']);
						} else {
							$result->Message = trans('app.CompletedSentencesSuccess', ['exp' => $exp, 'fish-icon' => '<img src="img/misc/fish.svg" alt="rybek" class="fish" />']);
						}
                    } else {
                    	$result->Message = trans('app.CompletedConversationSuccess', ['exp' => $exp, 'fish-icon' => '<img src="img/misc/fish.svg" alt="rybek" class="fish" />']);
                    }
				} else {
					throw new OnCompletedTaskException(trans('app.CouldnotCompleteTask'), ResponseCode::ERROR);	
				}
			} catch (ModelNotFoundException $e){
				$result->Status = ResponseCode::NOTFOUND;
				$result->Message = trans('app.NoRecordFound');
			} catch (OnCompletedTaskException $e){
                $result->Status = $e->getCode();
                $result->Message = $e->getMessage();
            }
			
		} else {
			$result->Status  = ResponseCode::ERROR;
			$result->Message = trans('app.WrongRequest');
		}
		
		return response()->json($result);
	}
}
