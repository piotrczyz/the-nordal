<?php

namespace Nordal\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Nordal\Definitions\ResponseCode;
use Nordal\Providers\MapServiceProvider;
use Nordal\Results\JsonResult;

class MapController extends Controller
{

	/**
	 * @var MapServiceProvider
	 */
	private $provider;
	
	public function __construct(MapServiceProvider $provider)
	{
		parent::__construct();
	
		$this->provider = $provider;
		$this->provider->SetUser(Auth::user());
	}

	public function GetMap(int $mapId = 0)
	{
		$result = new JsonResult();
		if ($this->provider->GetUser() === null){
			$result->Status = ResponseCode::ERROR;
			$result->Message = trans('app.NotAllowedUserAction');
		} else {

			try {
				$map = $this->provider->GetMap($mapId);
				$result->Status = ResponseCode::SUCCESS;
				$result->Result = $map;
			} catch (ModelNotFoundException $e){
				$result->Status = ResponseCode::ERROR;
				$result->Message = trans('app.NoRecordFound');
			} catch (AuthorizationException $e){
				$result->Status = $e->getCode();
				$result->Message = $e->getMessage();
			}
		}
		return response()->json($result);
	}
	
}
