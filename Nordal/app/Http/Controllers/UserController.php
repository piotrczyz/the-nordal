<?php

namespace Nordal\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nordal\User;

class UserController extends Controller
{   
	
	public function index()
	{
		$user = Auth::user();

		return view('user/profile', compact('user'))->with('IsMobile', $this->IsMobile);
	}

//	public function edit(User $user)
//	{
//		return view('user.profile',compact('user'));
//	}

	public function update(Request $request, User $user) {
		$this->validate($request, [
			'Name' => 'required',
			'Nickname' => 'required|unique:users'
		]);
		
		$user->update($request->all());
		return back();
	}

    //TODO @piotr Aksel: Ja tu tylko tak na szybko wysyłam sobie objekt użytkownika. Zbuduj to jak należy :)
    public function GetUser()
    {
        $user = Auth::user();

        if ($user === null){
            dd('No user');
        }

        return response()->json($user);
    }
}
