<?php

namespace Nordal\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Nordal\Definitions\ResponseCode;
use Nordal\Providers\MapServiceProvider;
use Nordal\Providers\StatsServiceProvider;
use Nordal\Results\JsonResult;

class StatsController extends Controller
{

	/**
	 * @var MapServiceProvider
	 */
	private $provider;
	
	public function __construct(StatsServiceProvider $provider)
	{
		parent::__construct();
	
		$this->provider = $provider;
		$this->provider->SetUser(Auth::user());
	}

	public function getTask(int $taskId)
	{
		$result = new JsonResult();
		if ($this->provider->GetUser() === null){
			$result->Status = ResponseCode::ERROR;
			$result->Message = trans('app.NotAllowedUserAction');
		} else {

			try {
				$task = $this->provider->getTask($taskId);
				$result->Status = ResponseCode::SUCCESS;
				$result->Result = $task;
			} catch (ModelNotFoundException $e){
				$result->Status = ResponseCode::ERROR;
				$result->Message = trans('app.NoRecordFound');
			} catch (AuthorizationException $e){
				$result->Status = $e->getCode();
				$result->Message = $e->getMessage();
			}
		}
		
		return response()->json($result);
	}

	public function task(int $taskId)
	{
		$taskStatsDto = $this->provider->getTask($taskId);

		return view('stats/task', compact('taskStatsDto'));
	}
}
