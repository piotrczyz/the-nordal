<?php

namespace Nordal\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
	
	public $IsMobile;

	public function __construct()
	{
		$this->middleware('auth');
		
		$detect = new \Mobile_Detect();
		$this->IsMobile = $detect->isMobile();
	}
}
