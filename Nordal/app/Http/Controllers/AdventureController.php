<?php

namespace Nordal\Http\Controllers;

class AdventureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /** @noinspection MagicMethodsValidityInspection */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the Angular JS adventure APP
     *
     */
    public function index()
    {
        return view('adventure')
            ->with('IsMobile', $this->IsMobile)
            ->with('IsAdventure', true);
    }
}
