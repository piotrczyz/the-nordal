<?php

namespace Nordal\Providers;

use DB;
use Nordal\Definitions\ResponseCode;
use Nordal\Dtos\PersonDto;
use Nordal\Dtos\TaskStatsDto;
use Nordal\User;
use Nordal\Models\Action;
use Nordal\Models\Task;

class StatsServiceProvider extends BaseServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->singleton(StatsServiceProvider::class, function ($app) {
		    return new StatsServiceProvider(config('stats'));
	    });
    }
    
    public function getTask(int $taskId)
    {
        $task = Task::find($taskId);
        $taskStatsDto = new TaskStatsDto();
        $taskStatsDto->TaskId = $task->id;
        $taskStatsDto->Name = $task->name;

        $doneBy = User::with('actions')
            ->join('actions', 'actions.user_id', '=', 'users.id')
            ->where('actions.task_id', $task->id)->orderBy('actions.created_at')->get();

        foreach($doneBy as $user) {
            $personDto = new PersonDto();
            $personDto->Name = $user->Name;
            $personDto->Nickname = $user->Nickname;
            $personDto->Avatar = $user->Avatar;
            $personDto->Gender = $user->Gender;
            $personDto->CharacterNumber = $user->CharacterNumber;

            $taskStatsDto->doneBy[] = $personDto;
        }

	    return $taskStatsDto;
    }
}
