<?php

namespace Nordal\Providers;

use DB;
use Illuminate\Auth\Access\AuthorizationException;
use Nordal\Definitions\ResponseCode;
use Nordal\Dtos\MapDto;
use Nordal\Dtos\PinDto;
use Nordal\Dtos\PersonDto;
use Nordal\Models\Action;
use Nordal\Models\Map;
use Nordal\Models\Task;
use Nordal\Definitions\PinType;

class MapServiceProvider extends BaseServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->singleton(MapServiceProvider::class, function ($app) {
		    return new MapServiceProvider(config('map'));
	    });
    }
    
    public function GetMap($mapId = 0) {
    	if (!($user = $this->GetUser())) {
    		throw new AuthorizationException(trans('app.NoUserContext'),ResponseCode::ERROR);
	    }

    	//TODO @piotr check if user has access to given map

        if ($mapId > 0 && Map::find($mapId) !== null) {
            $user->MapId = $mapId;
            $user->save();
        } else {
            // if the user does not have any map_id, we give him the first one
            if (!($user->MapId > 0)) {
                $user->MapId = Map::firstOrFail()->id;
                $user->save();
            }
            $mapId = $user->MapId;
        }
        // Check on wich chapter the user is right now
        $current_chapter = $user->updateAndGetCurrentChapter();
	    /** @var Map $map */
	    $map = Map::find($mapId);

        $dto = new MapDto();
        if ($map) {
            $dto->MapId = $map->Id;
            $dto->ParentMapId = $map->ParentMapId;
            $dto->AudioId = $map->AudioId;
            $dto->Map = $map->Name;
            $dto->MapUrl = $map->Url;
            $dto->Height = $map->Height;
            $dto->Width = $map->Width;
            $dto->Description = $map->Description;
            $dto->Pins = [];

            foreach ($this->getAvailablePins($current_chapter, $user->id, $map->id) as $pin) {
                $pinDto = new PinDto();
                $pinDto->PinId = $pin->id;
                $pinDto->PinType = $pin->type_name;
                $pinDto->Name = $pin->name;
                $pinDto->Coords = [$pin->x, $pin->y];
                $pinDto->Height = $pin->height;
                $pinDto->PinUrl = $pin->url;
                // if a pin is city, always true. If not, it's default disabled. If it has a task, it can be enabled
                $pinDto->IsEnabled = ($pinDto->PinType === PinType::CITY) ? true:false;
                if ($pin->t_id) {
                    $pinDto->TaskId = $pin->t_id;
                    $pinDto->IsEnabled = Task::isEnable($pin->t_id);
                    $pinDto->TasksAvailable = !Task::isDone($pin->t_id);
                }

                $pinDto->DestinationMapId = $pin->destination_map_id;
                $dto->Pins[] = $pinDto;
            }
            // check if there is any forced task
            // TODO Bierze tylko jedno zadanie isForced
            // TODO Jeżeli forced task jest w poprzednim chapterze, to go nie bierze?
            if (($tasks = Task::where('ChapterId', '<=', $current_chapter)->where('IsForced', true)->get())) {
                foreach ($tasks as $task) {
                    if (!$task->IsDone && $task->IsParentDone) {
                        if ($task->ParentTaskId) {
                            $task->ParentTask->Content = null;
                        }
                        $dto->ForcedTask = $task;
                        break;
                    }
                }
            }

            //TODO move to UserServiceProvider or PersonServiceProvider
            $dto->User = new PersonDto();
            $dto->User->Nickname = $user->Nickname;
            $dto->User->Gender = $user->Gender;
            $dto->User->CharacterNumber = $user->CharacterNumber;
            if ($lastTask = $user->Actions->last()) {
                $dto->User->LastTaskId = $lastTask->task_id;
            }
            $exp = DB::select('SELECT SUM(bests) as suma FROM (SELECT task_id, MAX(exp) as bests FROM actions WHERE user_id = ? GROUP BY task_id) AS bes;', array($user->id));

            $dto->User->Exp = (int)$exp[0]->suma;
        }
	    return $dto;
    }

    /**
     * Returns all pins on the given map, witch information about wich pins is available and witch already is done.
     * Takes tasks from the chapter on witch user is.
     *
     * @param int $chapterId
     * @param int $userId
     * @param int $mapId
     * @return array All available pins
     */
    private function getAvailablePins(int $chapterId, int $userId, int $mapId)
    {
        // Returning all pins, wich have a task available in users current chapter
        // In the system we have not implementet pisibility to have more that one task on a pins, that's why we group by
        // pin.id here
        $pins = DB::select('
SELECT * FROM pins
    left join (select pin_id, id as t_id from tasks where chapter_id = ?) as t on t.pin_id=pins.id
    left join (select task_id as a_task_id, count(task_id) as times_done from actions where user_id=? group by a_task_id) as a 
      on t.t_id=a.a_task_id 
    left join (select id, name as type_name from pin_types) as pt on pins.pin_type_id=pt.id
where map_id=?    
group by pins.id', array($chapterId, $userId, $mapId));

        return $pins;
    }
}
