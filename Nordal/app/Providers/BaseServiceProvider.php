<?php

namespace Nordal\Providers;

use Illuminate\Support\ServiceProvider;
use Nordal\User;

abstract class BaseServiceProvider extends ServiceProvider
{
	/** @var  User */
	protected $user;
		
	public function GetUser(){
		return $this->user;
	}   

	public function SetUser($user){
		$this->user = $user;
	}
}
