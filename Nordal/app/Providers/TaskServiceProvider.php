<?php

namespace Nordal\Providers;

use Nordal\Definitions\ResponseCode;
use Nordal\Definitions\TaskType;
use Nordal\Dtos\BubbleDto;
use Nordal\Dtos\ConversationDto;
use Nordal\Dtos\FableDto;
use Nordal\Dtos\PersonDto;
use Nordal\Dtos\SentenceDto;
use Nordal\Dtos\TaskCompleteDto;
use Nordal\Dtos\TaskDto;
use Nordal\Dtos\SentencesDto;
use Nordal\Exceptions\OnCompletedTaskException;
use Nordal\Exceptions\IncorrectTaskContentException;
use Nordal\Models\Action;
use Nordal\Models\Task;

class TaskServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(TaskServiceProvider::class, function ($app) {
			return new TaskServiceProvider(config('task'));
		});
	}
	
	public function CompleteTask(TaskCompleteDto $dto)
	{
		$response = [];
		$response['repeated'] = false;

		if ($dto->TaskId){
			/** @var TaskDto $task */
			$task =  $this->GetTask($dto->TaskId, true);
			$earnedExp = $this->_calculateExp($task, $dto);
			switch($task->Type){
                case TaskType::CHOOSE_CHARACTER:
					if (isset($dto->Data['CharacterNumber']) && ($ch = $dto->Data['CharacterNumber'])){
						$user = $this->GetUser();
						$user->Gender = strpos($ch, 'm') === 0 ? 'male' : 'female';
						$user->CharacterNumber = $ch;
                        $user->save();
					}
					break;
			}
			
			// if user alread completed task, find best result
			$bestExp = Action::where('TaskId',$dto->TaskId)->where('UserId', $this->GetUser()->Id)->max('Exp');

			if(is_numeric($bestExp)) {
				if ($task->IsRepeatable) {
					$response['repeated'] = true;
					$response['exp'] = 0;
					
					if($bestExp < $earnedExp) {
						$response['exp'] = $earnedExp - $bestExp;
					}
				} else {
					throw new OnCompletedTaskException(trans('app.AlreadyCompletedTask'),ResponseCode::WARNING);
				} 
			} else {
				$response['exp'] = $earnedExp;
			}
			$action = new Action();
			$action->TaskId = $dto->TaskId;
			$action->Exp = $earnedExp;
			$action->UserId = $this->GetUser()->Id;

			$action->save();
			
				
			return $response; // TODO może będziemy chcieli w przyszłości wyliczać Exp po stronie serwera i zwracać informacje użytkownikowi o zdobytych punktach doświadczenia
		}
		
		throw new OnCompletedTaskException(trans('app.WrongRequest'), ResponseCode::ERROR);
	}
	
	public function GetTask($taskId, $withOrgTask = false) {
//		echo '{"Status":10,"Message":null,"Result":{"TaskId":25,"Name":"\u0106wiczenia z Heidi","Description":null,"PinId":4,"Type":"sentences","Task":null,"IsEnabled":true,"IsDone":false,"IsRepeatable":true,"Exp":10,"ToLoadAudio":[""],"SentencesDto":{"IntroductionText":"Cze\u015b\u0107Ze mn\u0105 po\u0107wiczysz tworzenie zda\u0144 oznajmuj\u0105cych, do dzie\u0142a!","Sentences":[{"NativeSentence":"Tata siedzi w salonie.","AllWords":["stua","\u00e5 sitte","p\u00e5","papa","Pappa","i","sitter"],"CorrectOrderedWords":["Pappa","sitter","i","stua"]},{"NativeSentence":"Nelly cieszy si\u0119 na konferencj\u0119 letni\u0105 w Brunstad.","AllWords":["Brunstad","gleder","Nelly","gledet","sommerstevnet","til","seg","smile","p\u00e5"],"CorrectOrderedWords":["Nelly","gleder","seg","til","sommerstevnet","p\u00e5","Brunstad"]},{"NativeSentence":"Tata siedzi w salonie.","AllWords":["p\u00e5","i","\u00e5 sitte","papa","stua","Pappa","sitter"],"CorrectOrderedWords":["Pappa","sitter","i","stua"]}]}}}';
//		dd();
		/** @var Task $task */
		$task = Task::find($taskId);

        // Komentarz od Aksel:
        // Zadania typu fable i choose character mają pin_id = null, więc muszę to tutaj zakomentować i zmienić lekko na dole.
        // Jak to przeczytasz, to to usuń, jeżeli dobrze zrobiłem
        // Zmieniłem też: $dto->PinId = $task->Pin->Id;
        // na: $dto->PinId = !empty($task->Pin) ? $task->Pin->Id:null;
//      if ($task->Pin->Id === null){
//			//TODO throw an exception or log
//			return null;
//		}
		
		if ($task) {
			$dto       = new TaskDto();
			$dto->TaskId = $task->Id;
			$dto->Name = $task->Name;
			$dto->Description = $task->Description;
			$dto->PinId = !empty($task->Pin) ? $task->Pin->Id:null;
			$dto->IsEnabled = !$task->IsDone || $task->IsRepeatable;
			$dto->IsDone = $task->IsDone;
            $dto->IsRepeatable = (bool)$task->IsRepeatable;
            $dto->ToLoadAudio = explode(',', $task->ToLoadAudio);
			$dto->Type = $task->Type->Param1;
			if ($task->Type->Param1 === TaskType::CONVERSATION) {
				$dto->Conversation = $this->_getConversation($task->Content);
			}
            if ($task->Type->Param1 === TaskType::MESSAGE) {
                $dto->Message = $this->_getMessage($task->Content);
            }
			if ($task->Type->Param1 === TaskType::FABLE) {
				$dto->Fable = $this->_getFable($task->Content);
			}
			if ($task->Type->Param1 === TaskType::SENTENCES) {
				$dto->SentencesDto = $this->_getSentences($task->Content);
			}
			if ($withOrgTask) {
				$dto->Task = $task;
			}
			$dto->Exp = $task->Exp;

			return $dto;
		}
		// TODO @piotr jeżeli nie znajdzie zadania, to niech zwróci komunikat o tym, że nie znaleziono zadania o tym id, co myslisz?
		return null;
	}

	private function _getConversation($content){
		
		if (empty($content)){
			return [];
		}
		$user = $this->user;
		
		$replaceData = function($section)use($user) {
		
			$pattern = '/\[\[[a-z.:_\/]+\]\]/';
			
			if (preg_match($pattern, $section)){
				$start = strpos($section,'[[');
				$stop = strpos($section,']]');
				$str = substr($section, $start+2, $stop-$start-2);
				if(strpos($str,':') > 0) {
					$arr = explode(':', $str);
					if ($arr[0] === 'img_url'){ //path to /img folder
						$replacement = 'img' . '/' . $arr[1]; //TODO @piotr a global variable for img folder 	 
						$section = preg_replace($pattern, $replacement, $section);
					}
				} else
				if (strpos($str,'.') > 0){
					$arr = explode('.',$str);
					if ($arr[0] === 'user'){
						$replacement = $user->getAttribute($arr[1]); //TODO check if the property exists
						$section = preg_replace($pattern, $replacement, $section);
					}
				}  
				
			}
			return $section;
		};
		$lineNmb = 1;
		$parseLine = function ($line) use ($replaceData, &$lineNmb, $user){
			$sections = explode(';', $line);
			$ldto = new BubbleDto();
			$Person = new PersonDto();
			$Person->Name = $sections[0];
            $Person->Avatar = $replaceData($sections[1]);
            $Person->PersonType = $replaceData($sections[2]);
            $ldto->BubbleId = $lineNmb; //TODO @piotr możesz sobie wybrać inny sposób na generowanie ID Bubblów. Może być autoincrement
            $ldto->Person = $Person;
			$ldto->Alternatives = collect(explode('|',$sections[3]))->map(function($item)use($replaceData){
				return $replaceData($item);
			});
            if ($Person->PersonType === 'Player') {
                if (strpos($sections[4], '|') !== false) {
                    $ldto->Audio = [
                        ($user->Gender === 'male' ? explode("|",$sections[4])[0]:explode("|",$sections[4])[1]),
                        ($user->Gender === 'male' ? explode("|",$sections[5])[0]:explode("|",$sections[5])[1]),
                        ($user->Gender === 'male' ? explode("|",$sections[6])[0]:explode("|",$sections[6])[1])
                    ];
                } else {
                    $ldto->Audio = [
                        $sections[4],
                        $sections[5],
                        $sections[6],
                    ];
                }
                if (!empty($sections[7])) {
                    $ldto->CorrectAnswers = array_map('intval', explode(',', $sections[7]));
                }
            } else {
                $ldto->Audio = [$sections[4], $sections[5], $sections[6]];
            }

			$lineNmb++;			
			return $ldto;
		};
		
		$dialog          = new ConversationDto();
		$lines           = preg_split("/\\r\\n|\\r|\\n/", $content);
		$dialog->Bubbles = collect($lines)->map(function($line)use($parseLine){
			return $parseLine($line);
		});
		
		return $dialog;
	}

	private function _getMessage($content)
	{
		return $content;
	}

    private function _getFable($content)
    {
        $fable = new FableDto();
        $lines = preg_split("/\\r\\n|\\r|\\n/", $content);
        $fable->ButtonText = $lines[0];
        array_shift($lines); // deletes the first element of lines
        $fable->Content = implode('', $lines);
        return $fable;
    }

	private function _getSentences($content)
	{
		$sentences = new SentencesDto();
		$lines = preg_split("/\\r\\n|\\r|\\n/", $content);
		$sentences->IntroductionText = $lines[0];
		array_shift($lines); // deletes the first element of lines
		$i = 1;
		foreach ($lines as $line) {
			$parts = explode(';', $line);
			if (count($parts) !== 3) {
				throw new IncorrectTaskContentException(trans('app.IncorrectTaskContent', ['reason' => "Line $i does not have 3 parts separeted with a semicolon"]), ResponseCode::ERROR);
			}
			$sentence = new SentenceDto();
			$sentence->Id = $i;
			$sentence->NativeSentence = $parts[0];
			$sentence->AllWords = explode('|', $parts[1]);
			shuffle($sentence->AllWords);
			$sentence->CorrectOrderedWords = explode('|', $parts[2]);

			$sentences->Sentences[] = $sentence;
			$i++;
		}

		return $sentences;
	}
	
	private function _calculateExp(TaskDto $task, TaskCompleteDto $dto){
		$TotalExp = 0;
		
		switch ($task->Type){
			case TaskType::CONVERSATION:
				$answers = array_filter(isset($dto->Data['Answers']) ? $dto->Data['Answers'] : []);
				if (count($answers)){
					//all tasks which user had to answer for
					$TasksAnswers = collect($task->Conversation->Bubbles)->filter(function($bubble){
					    return count($bubble->CorrectAnswers);
					});
                    $NumberOfCorrectAnswers = [];
                    $NumberOfAlternatives = [];
                    foreach ($TasksAnswers as $bubbleDto){
                        /** @var $bubbleDto BubbleDto */
                        $NumberOfCorrectAnswers[$bubbleDto->BubbleId] = count($bubbleDto->CorrectAnswers);
                        $NumberOfAlternatives[$bubbleDto->BubbleId] = count($bubbleDto->Alternatives);
                    }
                    if (count($answers) != count($NumberOfCorrectAnswers)){
                        throw new OnCompletedTaskException(trans('app.WrongRequest'));
                    }
                    $BubblesNmb = count($answers); //number of bubbles
                    $FishesNmb = $task->Task->Exp; //total number of fishes in a task

                    $sum = 0.0;
                    foreach ($answers as $bubbleId => $usersAnswers){
                        $NumberOfIncorrectAlternatives = $NumberOfAlternatives[$bubbleId] - $NumberOfCorrectAnswers[$bubbleId];
                        $sum += ($NumberOfIncorrectAlternatives - (count($usersAnswers) - 1))/ $NumberOfIncorrectAlternatives;
                    }
                    $EarnedFishes = $sum / $BubblesNmb * $FishesNmb;
                    $TotalExp = ceil($EarnedFishes);
				} else {
					throw new OnCompletedTaskException(trans('app.NoAnswers'), ResponseCode::WARNING);
				}
				break;
			case TaskType::FABLE:
				$TotalExp = $task->Exp;
				break;
			case TaskType::SENTENCES:
				$answers = array_filter(isset($dto->Data['Answers']) ? $dto->Data['Answers'] : []);
				if (count($answers)){
					$wordsInSentence = [];
					foreach(collect($task->SentencesDto->Sentences) as $sentence) {
						$wordsInSentence[] = count($sentence->CorrectOrderedWords);
						$allWords[] = count($sentence->AllWords);
					}

					$sum = 0.0;
					$sentencesNmb = count($answers);

                    foreach ($answers as $sentenceId => $answer){
                    	$fails = count($answer)-$wordsInSentence[$sentenceId];
                        if($fails < 1) {
                        	$sum += 1;
                        } else if($fails<$allWords[$sentenceId]) {
                        	$sum += 1-$fails/$allWords[$sentenceId];
                        } else {
                        	$sum += 0;
                        }
                    }

					$TotalExp = floor($task->Exp*$sum/$sentencesNmb);
				} else {
					throw new OnCompletedTaskException(trans('app.NoAnswers'), ResponseCode::WARNING);
				}
				break;
		}
		return $TotalExp;
	}
}
