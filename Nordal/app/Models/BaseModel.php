<?php

namespace Nordal\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Mutable;

abstract class BaseModel extends Model
{
	use Eloquence;
	use Mappable, Mutable;
}
