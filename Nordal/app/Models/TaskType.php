<?php

namespace Nordal\Models;

/**
 * Class TaskType
 * @package Nordal\Models
 * 
 * @property int Id
 * @property string Name
 * @property string Param1
 */
class TaskType extends BaseModel
{
	public $timestamps = false;
	protected $table = 'tasks_types';
	protected $maps = [
		'Id' => 'id',
		'Name' => 'name',
		'Param1' => 'param1'
	];	
	protected $hidden = ['id','name','param1'];
	protected $appends = ['Id','Name','Param1'];
	
	public function Tasks(){
		
		return $this->hasMany(Task::class);
	}
}
