<?php

namespace Nordal\Models;
use Artisan;
use Carbon\Carbon;
use Nordal\Providers\TaskServiceProvider;

/**
 * Class Pin
 * 
 * @property int $Id
 * @property string $Name
 * @property double $X
 * @property double $Y
 * @property int $Height
 * @property boolean IsEnabled The user has passed all tasks from a previous chapter
 * @property boolean TasksAvailable There is a task available on or in this pin
 * 
 * @package Nordal\Models
 */
class Pin extends BaseModel
{
	public $timestamps = false;
	
//	protected $visible = ['id','name'];
	
	protected $with = ['PinType','Tasks'];
	
	protected $maps = [
        'Name' => 'name',
		'Id' => 'id',
		'X' => 'x',
		'Y' => 'y',
        'Height' => 'height',
        'Url' => 'url',
		'MapId' => 'map_id',
        'PinTypeId' => 'pin_type_id',
		'DestinationMapId' => 'destination_map_id'
	];	
	protected $hidden = ['id','name', 'x','y','height', 'url', 'map_id','pin_type_id', 'destination_map_id'];
	protected $appends = ['Id','Name','X','Y', 'Height', 'Url', 'IsEnabled', 'TasksAvailable', 'DestinationMapId'];
	
    public function Map(){
    	return $this->belongsTo(Map::class);
    }
	public function PinType(){
		return $this->belongsTo(PinType::class);
	}	
	public function Tasks(){
        //get a current chapter
        $now = Carbon::now();
        $last_chapter = 999;
        //TODO @piotr pobierac last chapter z jakiego service provider
        $chapters = Chapter::where('DateStart','<=', $now)->get();
        foreach ($chapters as $chapter){
            if (!$chapter->IsAccomplished){
                $last_chapter = $chapter->Id;
                break;
            }
            $last_chapter = $chapter->Id;
        }
		return $this->hasMany(Task::class)->where('ChapterId','<=',$last_chapter);
	}
    public function getTasksAvailableAttribute()
    {
        $TasksAvailable = false;
        if ($this->PinType->Name === 'city') {
            // TODO if in city are some tasks available, true should be returned
            // z tym się na razie tak nie śpieszy
            return false;
        } else {
            // If there are some tasks available
            foreach ($this->Tasks as $Task) {
                if (!$Task->IsDone) {
                    $TasksAvailable = true;
                }
            }
        }
        return $TasksAvailable;
    }

    /**
     * Shows only
     * @param $value
     * @return bool
     */
	public function getIsEnabledAttribute($value)
	{
		//TODO @piotr refactoring przy możliwych problemach z wydajnością
		//TODO @piotr trza to sprawdzić lub znaleźć lepszy sposób na sprawdzanie aktywnego Pin'u
		
		// 1. sprawdzić czy wszystkie zadania z poprzedniego etapu zostały wykonane
		// 2. sprawdzić czy dany objekt/pin/zadanie jest dostępne w obecnym tygodniu
        // 3. Rzucić jakiś wyjątek lub informację, jeżeli Chapter::where('Week', $currentWeek)->first() zwróci null

        $last_chapter = null;
		//get a current chapter
		$now = Carbon::now();
        $chapters = Chapter::where('DateStart','<=', $now)->get();
        foreach ($chapters as $chapter){
            if (!$chapter->IsAccomplished){
                $last_chapter = $chapter;
                break;
            }
            $last_chapter = $chapter;
        }


		$count = 0;
		$tasks = $this->Tasks; //wszystkie zadania dla Pin, ktore mają ustawiony ChapterId
		foreach ($tasks as $task){
			if ( $task->ChapterId == $last_chapter->Id && $task->IsParentDone){ //jeśli zadanie z poprzednich rozdziałów nie zostało wykonane
				$count++;
			}
		}
		// Jeżeli pin jest miastem, to ma być dostępny
		if ($this->PinType->Name === 'city') {
		    return true;
        } else {
            return $count > 0;
        }
	}

	public function GetAvailableTask(){

        $tasks = $this->Tasks;
        foreach ($tasks as $task) {
            /** @var $task Task */
            $parent = $task->ParentTask;
            $isParentDone = $parent ? $parent->IsDone : true;
            if (!$task->IsDone && $isParentDone){
                return $task;
            }
        }
        return null;
    }

    public function GetLastDoneTask(){

        $tasks = $this->Tasks()->orderBy('Id','DESC')->get();
        foreach ($tasks as $task) {
            /** @var $task Task */
            $parent = $task->ParentTask;
            $isParentDone = $parent ? $parent->IsDone : true;
            if ($task->IsDone && $isParentDone){
                return $task;
            }
        }
        return null;
    }

    public function GetTask(){

        $task = $this->GetAvailableTask();
        if ($task === null){
            $task = $this->GetLastDoneTask();
        }
        return $task;
    }

}
