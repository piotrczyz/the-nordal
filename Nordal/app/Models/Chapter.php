<?php

namespace Nordal\Models;

class Chapter extends BaseModel
{
	public $timestamps = false;
	
	protected $maps = [
		'Id' => 'id',
		'Name' => 'name',
		'Week' => 'week',
        'DateStart' => 'date_start'
	];
	protected $hidden = ['id','name','week','date_start'];
	protected $appends = ['Id','Name','Week','DateStart'];
	
	public function Words()
	{
		return $this->hasMany(Glossary::class);
	}

	public function Tasks(){
	    return $this->hasMany(Task::class)->where('ChapterId',$this->Id); //TODO czemu nie zadzialalo bez where?
    }

    public function getIsAccomplishedAttribute($value){
        $count = 0;
        $TasksCount = count($this->Tasks);
        foreach ($this->Tasks as $task){
            if ($task->IsDone){
                $count++;
            }
        }
        return $count === $TasksCount;
    }

}
