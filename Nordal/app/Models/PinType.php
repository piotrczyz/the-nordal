<?php

namespace Nordal\Models;

/**
 * Class Pin
 * 
 * @property int $Id
 * @property string $Name
 * 
 * @package Nordal\Models
 */
class PinType extends BaseModel
{
	public $timestamps = false;
	
//	protected $visible = ['id','name'];
	
    public function Pins()
    {
    	return $this->hasMany(Pin::class);
    }
    
    protected $maps = [
        'Id' => 'id',
	    'Name' => 'name'
    ];
	protected $hidden = ['id','name'];
	protected $appends = ['Id','Name'];
}
