<?php

namespace Nordal\Models;

/**
 * Class Map
 * 
 * @property $Id
 * @property $Name
 * @property $Url
 * @property $Description
 * @property Pin[] Pins
 * @package Nordal\Models
 */
class Map extends BaseModel
{
	public $timestamps = false;
	
	protected $maps = [
		'Id' => 'id',
		'Name' => 'name',
		'Url' => 'url',
        'Description' => 'description',
		'Height' => 'height',
		'Width' => 'width',
        'ParentMapId' => 'parent_map_id',
        'AudioId' => 'audio_id'
	];
	protected $hidden = ['id','name','url','description','parent_map_id','audio_id'];
	protected $appends = ['Id','Name','Url','Description','ParentMapId','AudioId'];
	
    public function Pins()
    {
    	return $this->hasMany(Pin::class);
    }
}
