<?php

namespace Nordal\Models;
use Illuminate\Support\Facades\Auth;

/**
 * Class Task
 * @package Nordal\Models
 * 
 * @property int $Id
 * @property string $Name
 * @property string $Description
 * @property int $Exp
 * @property boolean $IsRepeatable
 * @property int $TypeId
 * @property int $ParentTaskId
 * @property int $PinId
 * @property string $Content
 * @property bool $IsParentDone
 */
class Task extends BaseModel
{
	public $timestamps = false;
	
	protected $with = ['Type'];
	protected $maps = [
		'Id' => 'id',
		'Name' => 'name',
		'Description' => 'description',
		'Exp' => 'exp',
		'IsRepeatable' => 'repeatable',
		'TypeId' => 'type_id',
		'ParentTaskId' => 'parent_task_id', 
		'PinId' => 'pin_id',
		'ChapterId' => 'chapter_id',
		'Content' => 'content',
        'IsForced' => 'is_forced',
		'ToLoadAudio' => 'to_load_audio'
	];

	protected $hidden = ['id','name','exp','description', 'repeatable','type_id','parent_task_id','pin_id','chapter_id','content','is_forced','actions','to_load_audio'];
	protected $appends = ['Id', 'Name', 'Exp', 'Description','IsRepeatable','ParentTaskId','PinId', 'IsDone','ChapterId','Content','IsForced','ToLoadAudio'];
	
	public function Type(){
		return $this->belongsTo(TaskType::class);
	}
	public function Pin(){
		return $this->belongsTo(Pin::class);
	}
	public function Actions(){
		return $this->hasMany(Action::class)->Where('TaskId',$this->Id);
	}
	public function ParentTask(){
		return $this->belongsTo(Task::class);
	}
	public function Chapter(){
	    return $this->belongsTo(Chapter::class, 'TaskId');
    }
	public function getIsDoneAttribute($value)
	{
		// 1. sprawdzić czy zadanie zostało już wykonane
		$userId = Auth::user()->Id;
		return $this->actions->where('UserId', $userId)->count() > 0;
	}
	public function getIsParentDoneAttribute(){
        $parent = $this->ParentTask;
        return $parent ? $parent->IsDone : true; // in another words: if parent exists so check if it is done
    }
    public static function isEnable($taskId) {
        if ($task = Task::find($taskId)) {
            return $task->getIsParentDoneAttribute();
        }
        return false;
    }
    public static function isDone($taskId) {
        if ($task = Task::find($taskId)) {
            return $task->IsDone;
        }
        return false;
    }
}
