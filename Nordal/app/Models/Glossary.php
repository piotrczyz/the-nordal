<?php

namespace Nordal\Models;

class Glossary extends BaseModel
{
	public $timestamps = false;
	
    public function File()
    {
    	return $this->belongsTo('Nordal\Models\File');
    }
    
    public function Chapter()
    {
    	return $this->belongsTo('Nordal\Models\Chapter');
    }
}
