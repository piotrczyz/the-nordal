<?php

namespace Nordal\Models;
use Nordal\User;

/**
 * Class Pin
 * 
 * @property int $Id
 * @property int $UserId
 * @property int $TaskId
 * @property int $Exp
 * @property $CreatedDateTime
 * @property $UpdatedDateTime
 * 
 * @package Nordal\Models
 */
class Action extends BaseModel
{
	protected $maps = [
		'Id' => 'id',
		'UserId' => 'user_id',
		'TaskId' => 'task_id',
		'Exp' => 'exp',
		'CreatedDateTime' => 'created_at',
		'UpdatedDateTime' => 'updated_at'
	];	
	protected $hidden = ['id','user_id', 'task_id','exp','created_at','updated_at'];
	protected $appends = ['Id','UserId','TaskId','Exp','CreatedDateTime','UpdatedDateTime'];
	
    public function User()
    {
    	return $this->belongsTo(User::class);
    }

	public function Task()
	{
		return $this->belongsTo(Task::class);
	}
}
