<?php

namespace Nordal\Dtos;

class TaskStatsDto
{
	public $TaskId;
    public $DoneBy = [];
}