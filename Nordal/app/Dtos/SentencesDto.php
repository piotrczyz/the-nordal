<?php

namespace Nordal\Dtos;


class SentencesDto
{
	public $IntroductionText;
	/** @var [] SentenceDto */
    public $Sentences;
}

class SentenceDto {
	public $Id;
	public $NativeSentence;
	public $AllWords = [];
	public $CorrectOrderedWords = [];
}