<?php

namespace Nordal\Dtos;


class PersonDto
{
    public $Name;
    public $Nickname;
    public $Avatar;
    public $Gender;
    public $CharacterNumber;
    public $Exp;
    public $LastTaskId;
    public $PersonType; // 'Player' or 'Bot'
}