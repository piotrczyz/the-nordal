<?php

namespace Nordal\Dtos;

class TaskDto
{
	public $TaskId;
    public $Name;
    public $Description;
	public $PinId;
	public $Type;
	public $Task;
	public $IsEnabled;
	public $IsDone;
	public $IsRepeatable;
	public $Exp;
}