<?php

namespace Nordal\Dtos;


use Nordal\User;

class MapDto
{
	public $MapId;
    public $ParentMapId;
    public $AudioId;
	public $Map;
	/** @var User */
	public $User;
    public $MapUrl;
    public $Height;
    public $Width;
	public $Description;
	/** @var  PinDto [] */
	public $Pins = [];

}