<?php

namespace Nordal\Dtos;


class PinDto
{
	public $PinId;
	public $PinType;
	public $Name;
    public $Coords;
    public $Height;
    public $PinUrl;
	public $IsEnabled;
    public $TasksAvailable;
    public $TaskId;
    public $DestinationMapId;
}