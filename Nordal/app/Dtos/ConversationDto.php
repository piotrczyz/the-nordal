<?php

namespace Nordal\Dtos;


class ConversationDto
{
	/** @var [] BubbleDto */
	public $Bubbles;
} 

class BubbleDto {
	/** @var  PersonDto */
	public $Person;
    public $BubbleId;
	public $Alternatives = [];
    public $CorrectAnswers = [];
    public $Audio = [];
	
}