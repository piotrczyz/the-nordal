<?php

namespace Nordal;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nordal\Models\Map;
use Nordal\Models\Chapter;
use Nordal\Models\Action;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Mutable;
use DB;

/**
 * Class User
 * @package Nordal
 * 
 * @property int $Id
 * @property string $Name
 * @property string $Email
 * @property $Gender
 * @property $Birthday
 * @property string Nickname
 * @property string Avatar
 * @property int MapId
 */
class User extends Authenticatable
{
	use Eloquence;
	use Mappable, Mutable;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'name', 'email', 'Password', 'password', 'Gender', 'Birthday', 'Nickname', 'ChapterId'
    ];
	
	protected $maps = [
		'Id' => 'id',
		'Name' => 'name', 
		'Email' => 'email',
		'Gender' => 'gender',
		'Birthday' => 'birth_day',
		'Nickname' => 'nickname',
		'Password' => 'password',
		'Avatar' => 'avatar',
        'CharacterNumber' => 'character_number',
        'MapId' => 'map_id',
        'ChapterId' => 'chapter_id',
	];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','id','name','email','gender','birth_day','nickname','avatar','created_at','updated_at','character_number','map_id','chapter_id'
    ];
	protected $appends = ['Id','Name','Email','Gender','Birthday','Nickname','Avatar','CharacterNumber','MapId','ChapterId'];
	
	public function setBirthDayAttribute($value) {
		$this->attributes['birth_day'] = Carbon::parse($value)->format('Y-m-d');
	}

    public function Map() {
        return $this->belongsTo(Map::class);
    }

    public function Chapter() {
        return $this->belongsTo(Chapter::class);
    }

    public function Actions() {
        return $this->hasMany(Action::class);
    }

    public function updateAndGetCurrentChapter() {
        // get the current available chapter
        $now = Carbon::now();
        $current_chapter = Chapter::where('DateStart', '<=', $now)->get()->last();
        if ($current_chapter->id === $this->ChapterId) {
            return $this->ChapterId;
        }
        // count tasks in chapters
        $tasksInChapters = DB::select('
SELECT chapter_id, count(*) as cnt FROM tasks GROUP BY chapter_id ORDER BY chapter_id DESC');
        // Count how many tasks are done in chapters
        // TODO now we just count actions. If user has 2 entries for the same task, this will fail

        $actionsInLastChapter = DB::select('
SELECT chapter_id, count(DISTINCT t.id) as cnt FROM actions
  left join (select id, chapter_id from tasks) as t on actions.task_id=t.id
where user_id=? GROUP BY chapter_id ORDER BY chapter_id DESC LIMIT 1', array($this->id));

        if (count ($actionsInLastChapter)) {
            foreach ($tasksInChapters as $tasksInChapter) {
                if ($tasksInChapter->chapter_id === $actionsInLastChapter[0]->chapter_id) {
                    // If user has done as many tasks as it's needed, then we set his chapter_id to the next chapter
                    if ($tasksInChapter->cnt === $actionsInLastChapter[0]->cnt) {
                        $this->ChapterId = $tasksInChapter->chapter_id+1;
                        $this->save();
                    }
                    // We need to add this if, bacause when we added chapter_id to users table, some users already were in chapter 2
                    else if ($this->ChapterId !== $tasksInChapter->chapter_id) {
                        $this->ChapterId = $tasksInChapter->chapter_id;
                        $this->save();
                    }
                    return $this->ChapterId;
                }
            }
        }

        return $this->ChapterId;
    }
}
