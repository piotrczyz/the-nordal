<?php

namespace Nordal\Definitions;


class ResponseCode
{
	const SUCCESS = 10;
	const NOTFOUND = 20;
	const NOTALLOWED = 30;
	const ERROR = 99;
	const WARNING = 109;

}