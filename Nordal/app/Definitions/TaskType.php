<?php

namespace Nordal\Definitions;


class TaskType
{
	const CONVERSATION = 'conversation'; //konversacja z postaciami w grze
	const MESSAGE = 'message';
	const FABLE = 'fable'; //krótkie opowiadanie na początku każdego rozdziału
	const CHOOSE_CHARACTER = 'choose_character'; //wybieramy postać na początku gry
	const SENTENCES = 'sentences'; //wybieramy postać na początku gry
}