/**
 * Created by akselon on 2016-08-04.
 */
angular
    .module('advApp')
    .controller('chooseCharacterCtrl', ['$scope', '$state', '$stateParams', 'UserFactory', 'AudioFactory', 'soundtracks', 'TaskFactory',
        function ($scope, $state, $stateParams, UserFactory, AudioFactory, soundtracks, TaskFactory) {
        if (typeof $stateParams.taskId !== 'undefined' && $stateParams.taskId !== null) {
            $scope.isLoading = false;
            $scope.User = UserFactory;
            AudioFactory.loadAudio(soundtracks['GAME_THEME']).then(function () {
                AudioFactory.playAsBackground(soundtracks['GAME_THEME']);
            });

            $scope.saveCharacter = function () {
                $scope.isLoading = true;
                TaskFactory.completeTask($stateParams.taskId, {CharacterNumber: $scope.User.CharacterNumber}).then(function(task) {
                    // When user completes a fable task, redirect him
                    $state.go('main');
                    $scope.isLoading = false;
                }, function (reason) {
                    $scope.isLoading = false;
                    alertModal('Błąd zapisywania wyboru postaci', 'Niestety nie udało się zapisać wybranej postaci :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: '+reason);
                });
            }
        } else {
            console.log("No TaskId is defined. Redirect to main");
            $state.go('main');
        }
    }]);