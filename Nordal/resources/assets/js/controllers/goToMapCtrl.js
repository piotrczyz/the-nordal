/**
 * Created by akselon on 2016-09-16.
 */
angular
    .module('advApp')
    .controller('goToMapCtrl', ['$scope', '$state', '$stateParams', 'MapFactory', function ($scope, $state, $stateParams, MapFactory) {
        if (typeof $stateParams.mapId === 'undefined' || $stateParams.mapId === null) {
            console.log("No mapId is defined. Redirect to main");
            $state.go('main');
        } else {
            MapFactory.loadUsersMap($stateParams.mapId).then(function(map) {
                console.log("Zmieniamy mapę");
                $state.go('main'); // odświeżamy :)
                $scope.isLoading = false;
            }, function (reason) {
                $scope.isLoading = false;
                alertModal('Błąd zmiany mapy', 'Niestety nie udało się zmienić mapy :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: ' + reason);
            });
        }
    }]);