/**
 * Created by akselon on 2016-08-04.
 */
angular
    .module('advApp')
    .controller('fableCtrl', ['$scope', '$stateParams', '$state', '$sce', 'TaskFactory',
        function ($scope, $stateParams, $state, $sce, TaskFactory) {
        $scope.isLoading = true;
        if (typeof $stateParams.taskId === 'undefined' || $stateParams.taskId === null) {
            console.log("No TaskId is defined. Redirect to main");
            $state.go('main');
        } else {
            TaskFactory.getTask($stateParams.taskId).then(function(task) {
                if (task.Type == 'fable') {
                    $scope.task = task;
                    $scope.Content = $sce.trustAsHtml(task.Fable.Content);
                    $scope.ButtonText = task.Fable.ButtonText;
                } else {
                    alertModal('Błąd ładowania fabuły [#1]', 'Typ zadania nie jest fabułą.');
                }
                $scope.isLoading = false;
            }, function (reason) {
                alertModal('Błąd ładowania fabuły [#2]', 'Nie udało się załadować fabuły :( Prosimy o poinformowanie o tym administratora.<br/ >Więcej informacji: '+reason);
                $scope.isLoading = false;
            });

            $scope.completeFableTask = function () {
                $scope.isLoading = true;
                TaskFactory.completeTask($stateParams.taskId).then(function(task) {
                    // When use completes a fable task, redirect him
                    $state.go('main');
                    $scope.isLoading = false;
                }, function (reason) {
                    $scope.isLoading = false;
                    alertModal('Błąd oznaczania fabuły jako przeczytana', 'Niestety nie udało się oznaczyć fabuły jako przeczytana :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: '+reason);
                });
            }
        }
    }]);