/**
 * Created by akselon on 2016-09-10.
 */
angular
    .module('advApp')
    .controller('mainCtrl', ['$scope', '$state', '$stateParams', 'MapFactory', 'TaskFactory', 'AudioFactory', 'UserFactory',
        function ($scope, $state, $stateParams, MapFactory, TaskFactory, AudioFactory, UserFactory) {
        switch ($stateParams.action) {
            case 'task':
                console.log("mainCtrl: action = task");
                switch ($stateParams.parm1) {
                    case 'fable':
                        console.log("mainCtrl> fable");
                        $state.go('fable', {taskId: $stateParams.parm2});
                        break;
                    case 'choose_character':
                        console.log("mainCtrl> choose_character");
                        $state.go('chooseCharacter', {taskId: $stateParams.parm2});
                        break;
                    case 'conversation':
                        console.log("mainCtrl> conversation");
                        $state.go('taskConversation', {taskId: $stateParams.parm2});
                        break;
                    case 'sentences':
                        console.log("mainCtrl> sentences");
                        $state.go('taskSentences', {taskId: $stateParams.parm2});
                        break;
                    default:
                        alertModal('Błąd ładowania zadania [mainCtrl]', 'Ups, coś poszło nie tak. Nie można rozpoznać typu załadowanego zadania :(<br/>Typ: '+$stateParams.parm1);
                }
                break;
            default:
                MapFactory.loadUsersMap().then(function(map) {
                    UserFactory.updateInfo(map.User);
                    // If there is a ForceTask, we make it before we can go to the map
                    if (typeof map.ForcedTask !== 'undefined') {
                        console.log("mainCtrl> ForcedTask");
                        $state.go('main', {action: "task", parm1: map.ForcedTask.type.Param1, parm2: map.ForcedTask.Id});
                    } else {
                        console.log("mainCtrl> map");
                        $state.go('map');
                    }
                }, function (reason) {
                    alertModal('Błąd ładowania gry', 'Niestety nie udało się wczytać gry :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: '+reason);
                });

        }
    }]);