/**
 * Created by akselon on 2016-09-08.
 */
angular
    .module('advApp')
    .controller('advCtrl', ['$scope', 'AudioFactory', 'ngAudio', 'UserFactory', '$sce',
        function ($scope, AudioFactory, ngAudio, UserFactory, $sce) {
        $scope.audioIsMuted = false;
        $scope.User = UserFactory;
        $scope.muteAudio = function () {
            $scope.audioIsMuted = !$scope.audioIsMuted;
            if ($scope.audioIsMuted) {
                AudioFactory.mute();
            } else {
                AudioFactory.unmute();
            }
        };
    }]);