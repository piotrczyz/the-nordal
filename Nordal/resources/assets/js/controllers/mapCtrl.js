/**
 * Created by akselon on 2016-08-04.
 */
angular
    .module('advApp')
    .controller('mapCtrl', ['$scope', '$state', 'MapFactory', 'AudioFactory', 'soundtracks', 'TaskFactory', 'UserFactory',
        function ($scope, $state, MapFactory, AudioFactory, soundtracks, TaskFactory, UserFactory) {
        $scope.isLoading = true;
        $scope.pins = [];
        // Some initial values
        $scope.mapBg = {
            "background-image":"url('{{mapUrl}}')"
        };
        $scope.mapWidth = 100;
        $scope.mapHeight = 100;

        MapFactory.loadUsersMap().then(function(map) {
            if (typeof map.ForcedTask !== 'undefined') {
                $state.go('main', {action: "task", parm1: map.ForcedTask.type.Param1, parm2: map.ForcedTask.Id});
            }
            UserFactory.updateInfo(map.User);
            $scope.User = UserFactory;
            $scope.pins = map.Pins;
            $scope.mapWidth = map.Width;
            $scope.mapHeight = map.Height;
            $scope.mapBg["background-image"] = "url('"+map.MapUrl+"')";
            $scope.mapName = map.Description;
            $scope.parentMapId = map.ParentMapId;
            if ($scope.mapName === null || $scope.mapName === "") {
                alertModal('Błąd ładowania mapy', 'Baza danych zwróciła pustą mapę. Poinformuj proszę o tym administratora.');
            }
            if (typeof map.AudioId !== 'undefined' && map.AudioId !== null) {
                AudioFactory.loadAudio(map.AudioId).then(function (song) {
                    AudioFactory.playAsBackground(map.AudioId);
                });
            }

            $scope.pins.forEach(function (pin) {
                if (UserFactory.lastTaskId < 19 && pin.Name === "Jaskinia") {
                    pin.IsEnabled = false;
                }
                pin.isEnabled = pin.IsEnabled; // convention
                pin.cssClasses = pin.isEnabled ? "available" : "inaccessible";
                pin.startLeft = pin.left = pin.Coords[0];
                pin.startTop = pin.top = pin.Coords[1];
            });

            $scope.centerMap();
            $scope.isLoading = false;
        }, function (reason) {
            $scope.isLoading = false;
            alertModal('Błąd ładowania mapy', 'Niestety nie udało się wczytać mapy :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: '+reason);
        });

        /**
         * In that function we move all pins on the map when it's moved
         */
        $scope.movePins = function (backgroundX, backgroundY) {
            $scope.pins.forEach(function (pin) {
                pin.left = pin.startLeft + backgroundX;
                pin.top = pin.startTop + backgroundY;
            });
        };

        /**
         * Functions runned after a pind is clicked
         */
        $scope.pinClick = function () {
            if (this.pin.isEnabled && !$scope.isLoading) {
                pin = this.pin;
                pin.isClicked = true;
                // We do not allow to double click
                pin.isEnabled = false;
                $('#pin-id-'+pin.PinId).removeClass('available');
                $scope.isLoading = true;
                if (pin.PinType === 'city') {
                    MapFactory.loadUsersMap(pin.DestinationMapId).then(function(map) {
                        $state.go('main'); // odświeżamy :)
                        $scope.isLoading = false;
                    }, function (reason) {
                        $scope.isLoading = false;
                        alertModal('Błąd zmiany mapy', 'Niestety nie udało się zmienić mapy :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: ' + reason);
                    });
                } else {
                    TaskFactory.getTask(pin.TaskId).then(function (task) {
                        console.log('Wczytałem naciśnięte zadanie:');
                        console.log(task);
                        $state.go('main', {
                            action: "task",
                            parm1: task.Type,
                            parm2: pin.TaskId
                        });
                    }, function (reason) {
                        $scope.isLoading = false;
                        alertModal('Błąd wczytywania zadania', 'Niestety nie udało się wczytać zadania :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: ' + reason);
                    });
                }
            }
        };
    }]);