/**
 * Created by akselon on 2016-08-13.
 */
angular
    .module('advApp')
    .controller('sentencesCtrl', ['$state', '$stateParams', '$scope', '$timeout', 'TaskFactory', 'AudioFactory', 'UserFactory', '$location', '$sce',
        function ($state, $stateParams, $scope, $timeout, TaskFactory, AudioFactory, UserFactory, $location, $sce) {
        if (typeof $stateParams.taskId === 'undefined' || $stateParams.taskId === null) {
            console.log("No TaskId is defined. Redirect to main");
            $state.go('main');
        } else {
            $scope.User = UserFactory;
            // Ustawić na true podczas wczytywania czegokolwiek :)
            $scope.isLoading = true;
            // On the begining, we show introduction Text
            $scope.showSentences = false;
            $scope.introductionText = "";
            $scope.task = {};
            // isDone = true when the task is done. Used to show congratulation box
            $scope.taskDone = { isDone: false, message: "" };

            // Sentences to do
            // Important: sentences must be unique!! Inaczej ng-repeat nie działa
            sentences = [];
            // Words to choose
            $scope.wordsToChoose = [];
            // Words chosen
            wordsChosen = [];
            // The correct order
            correctOrderedWords = [];
            // Current sentence
            currentSentence = -1;
            // The sentence in native language
            $scope.nativeSentence = "";
            // The sentence made by the user
            $scope.foreginSentence = "";
            // If true, you will se a button to navigate to the next sentence
            $scope.showNextSentenceButton = false;
            // When answers are began to be send, this is true
            $scope.answersSend = false;

            /* Pobieramy zadanie i sprawdzamy czy zgadza się typ */
            TaskFactory.getTask($stateParams.taskId).then(function (task) {
                if (task.Type == 'sentences') {
                    $scope.task = task;
                    $scope.introductionText = $sce.trustAsHtml(task.SentencesDto.IntroductionText);
                    sentences = $scope.task.SentencesDto.Sentences;
                    makeSentencesStep();
                } else {
                    alertModal('Błąd ładowania zadania [#1]', 'Numer zadania przekazany w adresie URL nie jest zadaniem typu sentences.');
                    $scope.isLoading = false;
                }
            }, function (reason) {
                alertModal('Błąd ładowania zadania [#2]', 'Nie udało nam się załadować zadania :(<br/>Poinformuj proszę o tym administratora. Więcej informacji: ' + reason);
                $scope.isLoading = false;
            });

            makeSentencesStep = function () {
                if (typeof sentences !== 'undefined' && sentences.length > 0) {
                    sentenceReset();
                    var sentence = sentences.shift();
                    $scope.nativeSentence = $sce.trustAsHtml(sentence.NativeSentence);
                    $scope.wordsToChoose = sentence.AllWords;
                    correctOrderedWords = sentence.CorrectOrderedWords;
                    currentSentence++;
                    $scope.isLoading = false;
                } else {
                    $scope.isLoading = true;
                    $scope.answersSend = true;
                    TaskFactory.completeTask($stateParams.taskId, {Answers: wordsChosen}).then(function (task) {
                        console.log("Send");
                        console.log(task);
                        $scope.taskDone.message = $sce.trustAsHtml(task.Message);
                        $scope.taskDone.isDone = true;
                        $scope.isLoading = false;
                    }, function (reason) {
                        $scope.isLoading = false;
                        alertModal('Błąd wysyłania zadania', 'Niestety nie udało się wysłać Twoich odpowiedzi :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: ' + reason);
                    });
                }
            };

            sentenceReset = function () {
                $scope.nativeSentence = "";
                $scope.foreginSentence = "";
                $scope.wordsToChoose = [];
                $scope.showNextSentenceButton = false;
            }

            $scope.chooseWord = function (index, word) {
                if (wordsChosen[currentSentence] == undefined) {
                    wordsChosen[currentSentence] = [];
                }
                // Dodanie wypowiedzi do tabeli odpowiedzi
                wordsChosen[currentSentence].push(word);

                var correctWord = correctOrderedWords.shift();
                if (correctWord === word) {
                    correctAnswerAnimation('#word-' + index);
                    $scope.foreginSentence = $sce.trustAsHtml($scope.foreginSentence+word+" ");
                    $scope.wordsToChoose.splice(index-1, 1);
                    $('.eligible-word').removeClass('wrong-answer');
                    if (correctOrderedWords.length <= 0) {
                        $scope.showNextSentenceButton = true;
                    }
                } else {
                    wrongAnswerAnimation('#word-' + index);
                    correctOrderedWords.unshift(correctWord);
                }
            };
            function wrongAnswerAnimation(id) { $(id).addClass('wrong-answer'); }
            function correctAnswerAnimation(id) { $(id).addClass('correct-answer'); }

            $scope.nextSentence = function () {
                if (!$scope.answersSend) {
                    makeSentencesStep();
                }
            }
        }
        }]);