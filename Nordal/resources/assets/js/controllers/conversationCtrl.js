/**
 * Created by akselon on 2016-08-13.
 */
angular
    .module('advApp')
    .controller('conversationCtrl', ['$state', '$stateParams', '$scope', '$timeout', 'TaskFactory', 'AudioFactory', 'UserFactory', '$location', '$sce',
        function ($state, $stateParams, $scope, $timeout, TaskFactory, AudioFactory, UserFactory, $location, $sce) {

        if (typeof $stateParams.taskId === 'undefined' || $stateParams.taskId === null) {
            console.log("No TaskId is defined. Redirect to main");
            $state.go('main');
        } else {
            $scope.glued = false; //used to scroll down (se: https://github.com/Luegg/angularjs-scroll-glue)
            $scope.User = UserFactory;
            //Czas opóźnienia między wiadomościami
            delayTime = 1000;
            // Ustawić na true podczas wczytywania czegokolwiek :)
            $scope.isLoading = true;
            // Wykonywane zadanie
            $scope.task = {};
            // isDone = true when the task is done. Used to show congratulation box
            $scope.taskDone = {
                isDone: false,
                message: ""
            };
            // Zmienna przechowująca kolejne wiadomości
            // Ważne: conversation answers muszą być unikalne!! Inaczej ng-repeat nie działa
            conversation = [];

            /*
             Pobieramy zadanie i sprawdzamy czy zgadza się typ
             */
            TaskFactory.getTask($stateParams.taskId).then(function (task) {
                if (task.Type == 'conversation') {
                    $scope.task = task;
                    conversation = $scope.task.Conversation.Bubbles;
                    // Wait until all sounds are loaded
                    AudioFactory.loadSounds(task.ToLoadAudio).then(function () {
                        // If the task is already done, we only want to show the answers
                        if ($scope.task.IsDone) {
                            showMessages();
                            $scope.isLoading = false; // after showing
                        } else {
                            AudioFactory.stopBackground();
                            $scope.isLoading = false; // before first step
                            makeConversationStep();
                        }
                    }, function (reason) {
                        alertModal('Błąd ładowania dźwięków do zadania [#1]', 'Nie udało nam się załadować dźwięków :(<br/>Poinformuj proszę o tym administratora. Więcej informacji: ' + reason);
                        $scope.isLoading = false;
                    });
                } else {
                    alertModal('Błąd ładowania zadania [#1]', 'Numer zadania przekazany w adresie URL nie jest zadaniem typu konwersacji.');
                    $scope.isLoading = false;
                }
            }, function (reason) {
                alertModal('Błąd ładowania zadania [#2]', 'Nie udało nam się załadować zadania :(<br/>Poinformuj proszę o tym administratora. Więcej informacji: ' + reason);
                $scope.isLoading = false;
            });
            /*
             Wiadomości wyświetlane w chacie. To co tu będzie, jest wyświetlane
             */
            $scope.messages = [];
            // show and wait
            addMessage = function (name, avatar, cssClasses, content, audio) {
                var message = {};
                message.name = name;
                message.avatar = avatar;
                message.cssClasses = cssClasses;
                message.content = content;
                AudioFactory.playBubble(audio);

                $scope.messages.push(message);
            };
            // only show, without music
            addMessageFast = function (name, avatar, cssClasses, content) {
                var message = {};
                message.name = name;
                message.avatar = avatar;
                message.cssClasses = cssClasses;
                message.content = content;

                $scope.messages.push(message);
            };

            //Aktualne odpowiedzi dostępne do wyboru
            $scope.answers = [];
            //id Bombleka na który użytkownik właśnie odpowiada
            $scope.bubbleId = 0;
            // Wybrane odpowiedzi
            answersChosen = [];

            makeConversationStep = function () {
                if (typeof conversation !== 'undefined' && conversation.length > 0) {
                    var bubble = conversation.shift();
                    if (bubble.Alternatives.length > 1) {
                        $scope.bubbleId = bubble.BubbleId;
                        $scope.answers = bubble.Alternatives;
                        $scope.glued = false; // answers are shown, so we scroll used to the answers (glued goes false, idk why... meybe cos of animations?)

                        // we set the message back to conversation
                        conversation.unshift(bubble);
                    } else {
                        addMessage(
                            bubble.Person.Name,
                            bubble.Person.Avatar,
                            (bubble.Person.PersonType == 'Player') ? 'self' : 'other',
                            bubble.Alternatives[0],
                            bubble.Audio
                        );
                        $timeout(function () {
                            makeConversationStep();
                        }, (bubble.Audio[2] * 1000) + delayTime); //audio time + delayTime
                    }
                } else {
                    $scope.isLoading = true;
                    TaskFactory.completeTask($stateParams.taskId, {Answers: answersChosen}).then(function (task) {
                        $scope.taskDone.message = $sce.trustAsHtml(task.Message);
                        $scope.taskDone.isDone = true;
                        $scope.isLoading = false;
                        $scope.glued = false; // answers are shown, so we scroll used to the answers (glued goes false, idk why... meybe cos of animations?
                    }, function (reason) {
                        $scope.isLoading = false;
                        alertModal('Błąd wysyłania zadania', 'Niestety nie udało się wysłać Twoich odpowiedzi :( Poinformuj proszę o tym administratora.<br/ >Więcej informacji: ' + reason);
                    });
                }
            };

            function wrongAnswerAnimation(id) {
                $(id).addClass('wrong-answer');
            }

            function correctAnswerAnimation(id) {
                $(id).addClass('correct-answer');
            }

            $scope.chooseAnswer = function (index, bubbleId) {
                if (answersChosen[bubbleId] == undefined) {
                    answersChosen[bubbleId] = [];
                }
                // Dodanie wypowiedzi do tabeli odpowiedzi
                answersChosen[bubbleId].push(index);

                var bubble = conversation.shift();
                // Jeżeli odpowiedź była poprawna
                if (contains.call(bubble.CorrectAnswers, index)) {
                    correctAnswerAnimation('#' + index + "-" + bubbleId);
                    addMessage(
                        bubble.Person.Name,
                        bubble.Person.Avatar,
                        (bubble.Person.PersonType == 'Player') ? 'self' : 'other',
                        $scope.answers[index - 1],
                        bubble.Audio
                    );
                    $scope.answers = [];
                    $timeout(function () {
                        makeConversationStep();
                    }, (bubble.Audio[2] * 1000) + delayTime); //audio time + delayTime
                } else {
                    wrongAnswerAnimation('#' + index + "-" + bubbleId);
                    conversation.unshift(bubble);
                }
            };

            /**
             * If the user allready have done the conversation, we only want to display it
             */
            function showMessages() {
                if (typeof conversation !== 'undefined' && conversation.length > 0) {
                    var bubble = conversation.shift();
                    if (bubble.Alternatives.length > 1) {
                        addMessageFast(
                            bubble.Person.Name,
                            bubble.Person.Avatar,
                            (bubble.Person.PersonType == 'Player') ? 'self' : 'other',
                            bubble.Alternatives[bubble.CorrectAnswers[0]-1]
                        );
                    } else {
                        addMessageFast(
                            bubble.Person.Name,
                            bubble.Person.Avatar,
                            (bubble.Person.PersonType == 'Player') ? 'self' : 'other',
                            bubble.Alternatives[0]
                        );
                    }
                    showMessages();
                }
            }
        }
        }]);