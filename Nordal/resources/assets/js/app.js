// Load modules and directives
var advApp = angular.module('advApp', [
    'ui.router',
    'ngAnimate',
    'ngAudio',
    'advApp.directives.advMap',
    'luegg.directives'
]);

// Some constants
advApp.value("soundtracks",{
    GAME_THEME: 0,
    ZIBUR_THEME: 1
});
advApp.value('ngAudioGlobals', {
    unlock: true
})

// ur-router configuration
advApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/game');

    $stateProvider
        .state('main', {
            url: '/game',
            params: {
                action: null,
                parm1: null,
                parm2: null
            },
            controller: 'mainCtrl',
            templateUrl: 'templates/loading.html'
        })

        .state('fable', {
            url: '/fable',
            params: { taskId: null },
            controller: 'fableCtrl',
            templateUrl: 'templates/fable.html'
        })

        .state('chooseCharacter', {
            url: '/choose-character',
            params: { taskId: null },
            controller: 'chooseCharacterCtrl',
            templateUrl: 'templates/chooseCharacter.html'
        })

        .state('map', {
            url: '/map',
            controller: 'mapCtrl',
            templateUrl: 'templates/map.html'
        })

        .state('goToMap', {
            url: '/go-to-map/:mapId',
            controller: 'goToMapCtrl',
            templateUrl: 'templates/loading.html'
        })

        .state('taskConversation', {
            url: '/task/conversation',
            params: { taskId: null },
            controller: 'conversationCtrl',
            templateUrl: 'templates/tasks/conversation.html'
        })

        .state('taskSentences', {
            url: '/task/sentences',
            params: { taskId: null },
            controller: 'sentencesCtrl',
            templateUrl: 'templates/tasks/sentences.html'
        })
}]);