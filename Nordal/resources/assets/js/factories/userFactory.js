/**
 * Created by akselon on 2016-08-28.
 */

advApp.factory('UserFactory', ['$http', function($http) {
    return {
        nickname : 'Wiking',
        gender: 'female',
        characterNumber: 'f1',
        exp: -1,
        lastTaskId: -1,
        updateInfo : function (User) {
            this.nickname = (User.Nickname !== '') ? User.Nickname : 'Wiking';
            this.gender = User.Gender;
            this.characterNumber = User.CharacterNumber;
            this.exp = User.Exp;
            this.lastTaskId = User.LastTaskId;
        }
    };
}]);