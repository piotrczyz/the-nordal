/**
 * Created by akselon on 2016-09-08.
 */

advApp.factory('AudioFactory', ['$http', '$q', 'ngAudio', '$timeout', 'ngAudioGlobals', function($http, $q, ngAudio, $timeout, ngAudioGlobals) {
    // Loads all game soundtracks
    var audioLib = [
        // Chapter 1
        ngAudio.load('audio/soundtracks/game_theme2.mp3'),
        ngAudio.load('audio/soundtracks/zibur2.mp3'),
        ngAudio.load('audio/1/char_male.mp3'),
        ngAudio.load('audio/1/char_female.mp3'),
        ngAudio.load('audio/1/bot1.mp3'),
        ngAudio.load('audio/1/bot2.mp3'),
        ngAudio.load('audio/1/bot3.mp3'),
        ngAudio.load('audio/1/bot4.mp3'),
        // Chapter 2 from id=8
        ngAudio.load('audio/2/char_male.mp3'),
        ngAudio.load('audio/2/char_female_1.mp3'),
        ngAudio.load('audio/2/char_female_2.mp3'),
        ngAudio.load('audio/2/char_female_3.mp3'),
        ngAudio.load('audio/2/char_female_4.mp3'),
        ngAudio.load('audio/2/bot3.mp3'),
        ngAudio.load('audio/2/bot2.mp3'),
        ngAudio.load('audio/2/bot1.mp3'),
        ngAudio.load('audio/2/bot4.mp3'),
        // Chapter 3 from id=17
        ngAudio.load('audio/3/17_m_Heidi.mp3'),
        ngAudio.load('audio/3/18_m_Karczmarz.mp3'),
        ngAudio.load('audio/3/19_m_Sigve.mp3'),
        ngAudio.load('audio/3/20_f_Heidi.mp3'),
        ngAudio.load('audio/3/21_f_Karczamrz.mp3'),
        ngAudio.load('audio/3/22_f_Sigve.mp3'),
        ngAudio.load('audio/3/23_heidi.mp3'),
        ngAudio.load('audio/3/24_ivar.mp3'),
        ngAudio.load('audio/3/25_klassen.mp3'),
        ngAudio.load('audio/3/26_karczmarz.mp3'),
        ngAudio.load('audio/3/27_sigve.mp3'),
        // Chapter 3 (cave) from id=28
        ngAudio.load('audio/3/cave/28_m_goblin1.mp3'),
        ngAudio.load('audio/3/cave/29_m_goblin2.mp3'),
        ngAudio.load('audio/3/cave/30_m_goblin3.mp3'),
        ngAudio.load('audio/3/cave/31_f_goblin1.mp3'),
        ngAudio.load('audio/3/cave/32_f_goblin2.mp3'),
        ngAudio.load('audio/3/cave/33_f_goblin3.mp3'),
        ngAudio.load('audio/3/cave/34_goblin1.mp3'),
        ngAudio.load('audio/3/cave/35_goblin2.mp3'),
        ngAudio.load('audio/3/cave/36_golbin3.mp3'),
        ngAudio.load('audio/3/cave/37_cave_soundtrack.mp3'),
        // Chapter 5 from id = 38
        ngAudio.load('audio/5/38_male.mp3'),
        ngAudio.load('audio/5/39_female.mp3'),
        ngAudio.load('audio/5/40_sigve.mp3'),
        ngAudio.load('audio/5/41_jarl_torben.mp3'),
        ngAudio.load('audio/5/42_BlackWolfsInn.mp3'),
    ];
    // Background soundtrack
    var currentPlayingBgId = -1;
    // Global muting
    var isMuted = false;

    // see: https://github.com/danielstern/ngAudio/issues/16\
    // and: angular.audio.js:145 <- this pauses our background sounds, so we have to replay it
    window.addEventListener("click",function twiddle2(){
        if (currentPlayingBgId !== -1) {
            audioLib[currentPlayingBgId].play();
        }
        window.removeEventListener("click",twiddle2);
    });

    function _loadAudio (deferred, audioId) {
        return _loadAudioRecur (deferred, audioId, 0);
    }
    function _loadAudioRecur (deferred, audioId, callNum) {
        if (callNum > 99) {
            console.log("Sound loading time out");
            return deferred.reject("Sound loading time out");
        }
        setTimeout(function () {
            if (typeof audioLib[audioId].canPlay !== 'undefined') {
                if (audioLib[audioId].canPlay) {
                    return deferred.resolve(audioLib[audioId]);
                } else {
                    console.log("Sound: "+audioId+" could not be loaded.");
                    return deferred.reject("Sound: "+audioId+" could not be loaded.");
                }
            } else {
                return _loadAudioRecur (deferred, audioId, ++callNum);
            }
        }, 300);
    }
    function _loadAudioRecurCallBack (audioId, callNum, callBack) {
        if (callNum > 99) {
            console.log("Sound loading time out");
            return false;
        }
        setTimeout(function () {
            if (typeof audioLib[audioId].canPlay !== 'undefined') {
                if (audioLib[audioId].canPlay) {
                    callBack ();
                    return true;
                } else {
                    console.log("Sound: "+audioId+" could not be loaded.");
                    return false;
                }
            } else {
                return _loadAudioRecurCallBack (audioId, ++callNum, callBack);
            }
        }, 300);
    }

    return {
        loadAudio : function(audioId) {
            var deferred = $q.defer();
            if (typeof audioLib[audioId] === 'undefined'){
                console.log("Sound '"+audioId+"' could not be found in audio Library");
                return deferred.reject("Sound '"+audioId+"' could not be found in audio Library");
            }

            _loadAudio (deferred, audioId);

            return deferred.promise;
        },
        /*
         loadSounds działa w ten sposób:
         1) Odpalamy _loadAudioRecurCallBack dla wszystkich id piosenek w audiosToLoad
         2) Każda, kiedy się załaduje, odpala countCallBack
         3) countCallBack liczy ile razy już ktoś go odpalił
         4) Jeżeli odpalono go już tyle razy, ile jest piosenek w audiosToLoad, to znaczy, że można zwrócić deferred.resolve()
         */
        loadSounds : function(audiosToLoad) {
            var deferred = $q.defer();
            if( Object.prototype.toString.call( audiosToLoad ) === '[object Array]' ) {
                // TODO Jakiś semafor na ładowanie muzyk... Co jak 2 załadują się dokładnie w tym samym momencie?
                var toLoad = audiosToLoad.length;
                var loaded = 0;
                function countCallBack () {
                    loaded++;
                    if (loaded >= toLoad) {
                        return deferred.resolve();
                    }
                }
                audiosToLoad.forEach(function (audioId) {
                    if (typeof audioLib[audioId] === 'undefined'){
                        console.log("Sound '"+audioId+"' could not be found in audio Library");
                        return deferred.reject("Sound '"+audioId+"' could not be found in audio Library");
                    }
                    _loadAudioRecurCallBack (audioId, 0, countCallBack);
                });
            } else {
                console.log("`audiosToLoad` are not an array:");
                console.log(audiosToLoad);
                return deferred.reject("`audiosToLoad` are not an array");
            }

            return deferred.promise;
        },
        /*
         * @parm audio should be an array.
         * audio[0] - id of the mp3 file in audioLib array
         * audio[1] - time from witch we play the audio file
         * audio[2] - time when we pause the audio file
         */
        playBubble : function(audio) {
            if (audio.constructor !== Array || audio.length !== 3){
                console.log("Bubble:");
                console.log(audio);
                console.log("is not correct.");
            }

            this.loadAudio(audio[0]).then(function (song) {
                song.currentTime = audio[1];
                song.play();
                $timeout(function () {
                    song.pause();
                }, audio[2]*1000);
            });
        },
        mute: function () {
            isMuted = true;
            audioLib.forEach(function (audio) {
                audio.muting = true;
            })
        },
        unmute: function () {
            isMuted = false;
            audioLib.forEach(function (audio) {
                audio.muting = false;
            })
        },
        playAsBackground: function (audioId) {
            if (audioId !== currentPlayingBgId) {
                if (typeof audioLib[audioId] === 'undefined'){
                    console.log("Sound '"+audioId+"' could not be found in audio Library");
                    return false;
                }
                if (currentPlayingBgId !== -1) {
                    audioLib[currentPlayingBgId].stop();
                }
                currentPlayingBgId = audioId;
                if (!isMuted) {
                    audioLib[audioId].volume = 0.5;
                    audioLib[audioId].muting = false;
                }
                audioLib[audioId].play();
            }
        },
        stopBackground: function () {
            if (currentPlayingBgId !== -1) {
                audioLib[currentPlayingBgId].stop();
                currentPlayingBgId = -1;
            }
        }
    }
}]);