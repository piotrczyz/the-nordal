/**
 * Created by akselon on 2016-08-28.
 */

advApp.factory('TaskFactory', ['$http', '$q', function($http, $q) {
    var urlBase = 'task/';
    return {
        getTask : function(taskId) {
            var deferred = $q.defer();
            $http.get(urlBase+"get/"+taskId).then(function (response) {
                if (response.data.Status == 10) {
                    return deferred.resolve(response.data['Result']);
                } else {
                    console.log("response.data.Status is not 10");
                    return deferred.reject("response.data.Status is not 10");
                }
            }, function (error) {
                console.log("Chould not resolve $http request");
                return deferred.reject("Chould not resolve $http request");
            });
            return deferred.promise;
        },
        completeTask : function(taskId, dataToSend) {
            var deferred = $q.defer();
            var req = {
                method: 'POST',
                url: urlBase+"complete",
                data: {
                    TaskId: taskId,
                    Data: dataToSend
                }
            };
            $http(req).then(function (response) {
                if (response.data.Status == 10) {
                    return deferred.resolve(response.data);
                } else {
                    console.log("response.data.Status is not 10");
                    console.log(response);
                    return deferred.reject("response.data.Status is not 10<br/>"+response.data);
                }
            }, function (error) {
                console.log("Chould not resolve $http request. Error message:");
                console.log(error);
                return deferred.reject(error.data);
            });
            return deferred.promise;
        }
    };
}]);