/**
 * Created by akselon on 2016-08-28.
 */

advApp.factory('MapFactory', ['$http', '$q', function($http, $q) {
    return {
        loadUsersMap : function(mapId) {
            var deferred = $q.defer();
            mapId = (typeof mapId === 'undefined') ? '':mapId;
            $http.get('map/get/'+mapId).then(function (response) {
                if (response.data.Status == 10) {
                    return deferred.resolve(response.data['Result']);
                } else {
                    console.log("response.data.Status is not 10");
                    return deferred.reject("response.data.Status is not 10");
                }
            }, function (error) {
                console.log("Chould not resolve $http request");
                return deferred.reject("Chould not resolve $http request");
            });
            return deferred.promise;
        },
        changeMap : function(mapId) {
            var deferred = $q.defer();
            var req = {
                method: 'POST',
                url: "map/change",
                data: {
                    MapId: mapId
                }
            };

            $http(req).then(function (response) {
                if (response.data.Status == 10) {
                    return deferred.resolve(response.data);
                } else {
                    return deferred.reject("response.data.Status is not 10");
                }
            }, function (error) {
                return deferred.reject("Chould not resolve $http request");
            });
            return deferred.promise;
        }
    }
}]);