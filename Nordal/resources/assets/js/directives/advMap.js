/**
 * Created by akselon on 2016-07-28.
 */
angular.module('advApp.directives.advMap', []).
directive('advMap', ['$document', function($document) {
    return {
        restrict: 'E',
        link: function(scope, element, attr) {
            var deltaX = 0, deltaY = 0,
                backgroundX = 0, backgroundY = 0,
                prevBackgroundX = 0, prevBackgroundY = 0,
                startX = 0, startY = 0,
                boxWidth = element.width(), boxHeight = element.height();

            // Center the map
            scope.centerMap = function () {
                var deltaX = scope.mapWidth - boxWidth;
                backgroundX = prevBackgroundX = (deltaX < 0) ? 0 : Math.floor(-deltaX/2);
                var deltaY = scope.mapHeight - boxHeight;
                backgroundY = prevBackgroundY = (deltaY < 0) ? 0 : Math.floor(-deltaY/2);
                scope.mapBg["background-position"] = backgroundX+"px "+backgroundY+"px";
                scope.movePins(backgroundX, backgroundY);
            };
            element.draggable({
                delay: 50, //prevents from draging when the user wants to click on a pin
                start: function (event, ui) {
                    startX = event.screenX;
                    startY = event.screenY;
                },
                drag: function (event, ui) {
                    // Warning: the drag function is executed when mouse coordinates change. But be aware!
                    // It is NOT executed each one pixel change. So it can be executed after X axis had changed
                    // 50 pixels! Thats wy we jave to set coordinates back to start position

                    ui.position.top = 0; ui.position.left = 0;// Prevent from dragging the whole map element

                    deltaX = event.screenX - startX;
                    deltaY = event.screenY - startY;
                    backgroundX = backgroundX+deltaX;
                    backgroundY = backgroundY+deltaY;
                    startX = event.screenX; startY = event.screenY;
                    if (backgroundX > 0) {backgroundX = 0;}
                    if (backgroundY > 0) {backgroundY = 0;}
                    if (backgroundX < boxWidth-scope.mapWidth) {backgroundX = boxWidth-scope.mapWidth;}
                    if (backgroundY < boxHeight-scope.mapHeight) {backgroundY = boxHeight-scope.mapHeight;}
                    scope.mapBg["background-position"] = backgroundX+"px "+backgroundY+"px";

                    // If map coordinates changed, we have to move all the Pins
                    if (backgroundX !== prevBackgroundX || backgroundY !== prevBackgroundY) {
                        scope.movePins(backgroundX, backgroundY);
                    }
                    prevBackgroundX = backgroundX; prevBackgroundY = backgroundY;
                }
            });
        },
        templateUrl: "templates/directives/advMap.html",
        replace: true
    };
}]);