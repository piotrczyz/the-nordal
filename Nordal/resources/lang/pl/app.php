<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Teksty z gry
	|--------------------------------------------------------------------------
	*/

	/* PROFIL */
    'LoginBtn' => 'Zaloguj',
	'Login' => 'Logowanie',
	'Register' => 'Zarejestruj',
	'Registration' => 'Rejestracja',
	'male' => 'Wiking',
	'female' => 'Kobieta wiking',
	'Name' => 'Imię i Nazwisko',
	'Gender' => 'Płeć',
	'Birthday' => 'Data urodzin',
	'Email' => 'E-mail',
	'Save' => 'Zapisz',
	'Nickname' => 'Ksywka',
	
	'ProfilHeading' => 'Edytuj swój profil',
	'EditProfile' => 'Edytuj profil',
	'Logout' => 'Wyloguj',
	'Password' => 'Hasło',
	'ConfirmPassword' => 'Potwierdź hasło',
	
	'NoRecordFound' => 'Nie znaleziono rekordu',
	'WrongRequest' => 'Błędne lub niekompletne zapytanie',
	'AlreadyCompletedTask' => 'Zadanie już zostało wykonane wcześniej',
	'CompletedRepeatedSentencesSuccess' => 'To zadanie wykonałeś już po raz kolejny! Otrzymujesz dodatkowo :exp :fish-icon!',
	'CompletedSentencesSuccess' => 'Za ułożenie zdań otrzymujesz :exp :fish-icon!',
	'CompletedConversationSuccess' => 'Za tę rozmowę otrzymujesz :exp :fish-icon!',
	'NotAllowedUserAction' => 'Nie można wykonać akcji',
	'CouldnotCompleteTask' => 'Nie udało się wykonać zadania',
	
	/* TASK */
	'NoAnswers' => 'Nie podano żadnych odpowiedzi',
	'IncorrectTaskContent' => 'Pole content w bazie danych dla tego zadania jest niepoprawne. Powód: :reason',
	
];