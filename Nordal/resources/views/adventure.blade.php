@extends('layouts.app')

@section('content')
    <!-- THIS IS WHERE WE WILL INJECT OUR CONTENT ============================== -->
    <div ui-view></div>
@endsection

@section('scripts')
    <script src="{{ elixir('js/adventure.js') }}"></script>
@endsection