<!-- NAVIGATION -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top affix-top bar-texture">
    <div class="container" style="position: relative;">
        <a class="navbar-brand" href="/">
            <img src="<?=asset('assets/nordal-logo-small-shadow.png')?>" alt="Logo Nordal"/>
        </a>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-bars fa-6" aria-hidden="true"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <?php if (Auth::guest()) { ?>
                        <li><a href="<?= url('/login') ?>"><?= trans('app.Login') ?></a></li>
                        <li><a href="<?= url('/register') ?>"><?= trans('app.Registration') ?></a></li>
                    <?php } else { ?>
                        <li><a href="<?=route('adventure')?>">Przygoda</a></li>
                        <li><a href="<?= url('user/profile') ?>"><?= trans('app.EditProfile') ?></a></li>
                        <li><a href="<?= url('logout') ?>"><?= trans('app.Logout') ?></a></li>
                    <?php } ?>
                </ul>
            </li>
        </ul>
        <div id="audio-controll" ng-click="muteAudio()">
            <i class="fa" aria-hidden="true" ng-class="{'fa-volume-up': !audioIsMuted, 'fa-volume-off': audioIsMuted}"></i>
        </div>
        <?php if (isset($IsAdventure) && $IsAdventure) { ?>
            <div id="users-fishes" ng-show="User.exp !== -1">
                <img src="img/misc/fish.svg" alt="Twoje Rybki - Twoje bogactwo" class="fish" />{{User.exp}}
            </div>
        <?php } ?>
    </div>
</nav>