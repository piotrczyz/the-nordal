@extends('layouts/app')

@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading">{{ trans('app.ProfilHeading') }}</div>

			<div class="panel-body">


				{!! Form::model($user, ['url' => "user/{$user->Id}", 'action' => 'POST']) !!}
				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				<div class="form-group">
					{!! Form::label('Email', trans('app.Email')) !!}
					{!! Form::email('Email', $user->Email, ['class' => 'form-control', 'readonly']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('Name', trans('app.Name')) !!}
					{!! Form::text('Name', $user->Name, ['class' => 'form-control']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('Nickname', trans('app.Nickname')) !!}
					{!! Form::text('Nickname', $user->Nickname, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('Birthday', trans('app.Birthday')) !!}
					{!! Form::text('Birthday', $user->Birthday, ['class' => 'form-control datapicker']) !!}

				</div>
				<script>
					$(".datepicker").datepicker({
						yearRange: "-100:+0"
					});
				</script>
				<div class="form-group">
					{!! Form::submit(trans('app.Save'), ['class' => 'btn btn-primary form-control']) !!}
				</div>

				{!! Form::close() !!}

				@if (count($errors))
					<ul>
					@foreach ($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
					</ul>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection