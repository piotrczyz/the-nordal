@extends('layouts/app')

@section('content')
<div class="row">
		<style>
			table {
				border-collapse: collapse;
				width: 100%;
			}

			th, td {
				text-align: left;
				padding: 8px;
			}

			.even {
				background-color: #f2f2f2;
			}
			img {
			   position: relative;
			   left: 50%;
			   -webkit-transform: translateX(-50%);
			}
		</style>

	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading">
				Wikingowie którzy ukończyli misję <b>Powstań i walcz</b>
			</div>
			<div class="panel-body">
				{{ Html::image('img/missions/other/piw_transparent.png', 'Powstań i walcz', array('style'=>'height: 100px;')) }}
				@if (!empty($taskStatsDto->doneBy))
					<table style="padding: 20px 0;">
						@foreach($taskStatsDto->doneBy as $key => $user)
							<tr
								@if($key%2==0)
									class='even'
								@endif
							>
								<td>{{ $key+1 }}</td>
								<td>{{ Html::image("img/characters/thumbs/{$user->CharacterNumber}.png", 'Powstań i walcz', array('style'=>'height: 50px;')) }}</td>
								<td>{{ $user->Name }} <?= !empty($user->Nickname) ? "({$user->Nickname})":"" ?></td>
							<tr>
						@endforeach
					</table>
				@else
					<p style="text-align: center; margin: 20px 0;">
						<i>Żaden wiking nie ukończył jeszcze tej misji.</i>
					</p>
				@endif
				<br />
				Aby móc przystąpić do misji <b>Powstań i walcz</b>, należy wykonać zadania z tygodni 1, 2 i 3. <br />
				<a href="{{url('')}}/adventure#/map"><i class="fa fa-arrow-left" aria-hidden="true"></i> Powrót do mapy</a>
			</div>
		</div>
	</div>
</div>
@endsection