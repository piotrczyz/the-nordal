@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Witamy w krainie Nordalu!</div>

        <div class="panel-body" style="position: relative; font-size: 18px;">
            <div>
                <p>
                    Gra ta została stworzona po to, aby pomóc ci w nauce norweskiego - najważniejszego języka w zborze. Regularnie będą tutaj dodawane nowe zadania, tak aby można było ćwiczyć swoje umiejętności pomiędzy lekcjami prowadzonymi na kursie. Znajomość norweskiego jest niesłychanie wielką pomocą - pomyśl, móc samemu rozumieć mowę braci i mieć społeczność z przyjaciółmi na całym świecie!
                </p>
                <p>
                    Teraz możesz grać online razem ze swoimi przyjaciółmi!
                </p>
                <div class="text-center" style="margin-top: 50px">
                    <a href="{{route('adventure')}}" class="btn btn-lg btn-success adv-btn">Rozpocznij przygodę <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
@endsection
