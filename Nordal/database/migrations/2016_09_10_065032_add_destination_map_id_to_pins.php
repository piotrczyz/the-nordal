<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDestinationMapIdToPins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pins', function (Blueprint $table) {
            $table->integer('destination_map_id')->unsigned()->nullable();
            $table->foreign('destination_map_id')->references('id')->on('maps')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pins', function (Blueprint $table) {
            $table->dropForeign('pins_destination_map_id_foreign');
            $table->dropColumn('destination_map_id');
        });
    }
}
