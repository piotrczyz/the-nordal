<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();	        
	        $table->integer('exp');	   
        });
	    
	    Schema::table('actions', function(Blueprint $table){
		    $table->integer('user_id')->unsigned();
		    $table->foreign('user_id')->references('id')->on('users');
		    $table->integer('task_id')->unsigned();
		    $table->foreign('task_id')->references('id')->on('tasks');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('actions', function(Blueprint $table){
    		$table->dropForeign('actions_user_id_foreign');
    		$table->dropForeign('actions_task_id_foreign');
	    });
        Schema::drop('actions');
    }
}
