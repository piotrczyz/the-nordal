<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAudioIdToMaps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('maps', function(Blueprint $table) {
            $table->integer('audio_id')->nullable();
        });
        $zibur_city = \Nordal\Models\Map::where('name', 'zibur')->firstOrFail();
        $zibur_city->AudioId = 1;
        $zibur_city->save();
        $noral = \Nordal\Models\Map::where('name', 'nordal')->firstOrFail();
        $noral->AudioId = 0;
        $noral->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('maps', function (Blueprint $table) {
            $table->dropColumn('audio_id');
        });
    }
}
