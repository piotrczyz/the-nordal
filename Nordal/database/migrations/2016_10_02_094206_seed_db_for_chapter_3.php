<?php

use Illuminate\Database\Migrations\Migration;

class SeedDbForChapter3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Najpierw standardowe zadania
        $fableTaskTypeId = DB::table('tasks_types')->where('param1', 'fable')->value('id');
        $conversationTaskTypeId = DB::table('tasks_types')->where('param1', 'conversation')->value('id');
        $Chapter3Id = DB::table('chapters')->where('name', 'Etap 3')->value('id');

        $botKarczmarzId = DB::table('pins')->where('name', 'Karczmarz')->value('id');
        $botIvarId = DB::table('pins')->where('name', 'Ivar')->value('id');
	    $botHeidiId = DB::table('pins')->where('name', 'Heidi')->value('id');
        $botSigveId = DB::table('pins')->where('name', 'Sigve')->value('id');

        $lastTaskId = DB::table('tasks')->orderBy('id', 'desc')->first()->id;

        $fableId = DB::table('tasks')->insertGetId(['name'=>'Szkolenie to długi, ale potrzebny proces', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$Chapter3Id, 'is_forced'=>1, 'parent_task_id' => $lastTaskId, 'content' => 'Pokaż mapę
<p>To jest bardzo długi dzień… Masz już za sobą dwie godziny nauki o łodziach, godzinę nawigacji i dwie godziny teorii taktyki bojowej (która swoją drogą okazała się niesamowicie nudnym i dłużącym się przedmiotem), a czekają cię jeszcze godzina zielarstwa i w końcu długo wyczekiwana pierwsza lekcja szermierki. Ale nie czuj się znudzony i zrezygnowany! Wiedz, że cały ten trud może ci się przydać kiedy zaczną się zajęcia praktyczne i pierwsze wyprawy, a to może nastąpić już wkrótce!</p>
']);
        $task1Id = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Heidi','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'pin_id'=>$botHeidiId, 'parent_task_id' => $fableId, 'to_load_audio' => '17,20,23',
            'content' =>'Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Hei!;23;0.8;0.6
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Gudag, Heidi!|Good dag, Heidi!|God dag, Heidi!;17|20;1|0.8;1.2|1.2;3
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Vil du jogge med meg?;23;2.3;1.4
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Gjerne, men jeg har ikke tid.|Jarne, men jeg er ikke tid.|Gjarne, men jeg har ingenting tiden.;17|20;4.8|2.65;3.5|3;1
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Hva skal du gjøre?;23;5.1;1.1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg skal lærer om pflanten og om krigen.|Jeg skal lære om planter og om krigen.|Jeg skal larer om plante og om krigen.;17|20;11|7.1;4|2.8;2
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Det høres spennende ut.;23;7.7;2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, men jeg er litt redd.|Ja, men jeg er lit red.|Ja, men jeg er reddet.;17|20;18.5|11.4;3|2.6;1
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Bli ikke trist, læreren skal være tålmodig.;23;11.0;3.8
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Du rett. Jeg mo gå.|Du har rett. Jeg må gå.|Du er ret. Jeg mo go.;17|20;25|15.5;2.3|2.3;2
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Lykke til!;23;17;1.1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Tak! Ha dee!|Takk! Ha det!|Takket! Hav det!;17|20;29|18.5;2.5|2;2']);
        $task2Id = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Ivarem','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'pin_id'=>$botIvarId, 'parent_task_id' => $task1Id, 'to_load_audio' => '24,25',
            'content' =>'Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Hei alle sammen! ;24;1.1;1.2
Klassen;img/map_objects/Zibur/avatars/class.png;Player;God dag, lærer!;25;0.6;2.2
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Jeg skal lære dere om krigen, og jeg håper at dere skal ikke kjede dere.;24;3.5;4.8
Klassen;img/map_objects/Zibur/avatars/class.png;Player;Vi gleder oss.|Vi glade oss!|Vi kan ikke venter!;25;3.8;1.9;1
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Men først må vi ha sterke armer. Er dere sterke?;24;8.75;4.3
Klassen;img/map_objects/Zibur/avatars/class.png;Player;Ja, vi er veldi sterk!|Ja, men vi må trene litt mere.|Ja, men vi er longt ikke nok sterk.;25;6.6;3.4;2']);
        $task3Id = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Karczmarzem','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'pin_id'=>$botKarczmarzId, 'parent_task_id' => $task2Id, 'to_load_audio' => '18,21,26',
            'content' =>'Wiking;img/characters/thumbs/[[user.character_number]].png;Player;God dag, Telan! Hvordan går det med deg?|God dag, Telan! Hvurdan gor det med deg?|God dag, Telan! Hvordan gå det me deg?;18|21;0.6|0.8;4|3;1
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Bare bra, takk.;26;0.3;1.9
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Kan du ikke lager maten?|Kan du lage mat for meg?|Lag mat får meg no.;18|21;7.5|5.2;2|1.8;2
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Selvfølgelig! Det er min jobb. Hva har du lyst å spise?;26;2.5;5.5
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Et stort middag, tak!|Kan jeg en stort middag?|Kan jeg få en stor middag?;18|21;12|8.8;2.5|3.3;3
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Det kan du.;26;9.3;1.6']);
        $task4Id = DB::table('tasks')->insertGetId(['name'=>'Rozmowa ze starcem Sigve','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'pin_id'=>$botSigveId, 'parent_task_id' => $task3Id, 'to_load_audio' => '19,22,27',
            'content' =>'Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Hei! Hva har du gjort i dag?;27;0.6;2.5;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;I morgen tok jeg klær på meg og spilte frokost.|I morges har jeg kledd klær på meg og spist frokost.|Om morgen taet jeg klær på seg og spisset frokost.;19|22;0.6|1.2;4.2|4.3;2
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Og hva har du gjort i ettermiddagen?;27;4.5;3;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;På ettermidda har jeg møtet mine vennene, og hatt mange leksjoner..|I ettermiddagen har jeg møtt mine venner, og jeg hadde mange leksjoner..|I ettermiddag har jeg møtetet venner mine, og hatt mange leksjoner...;19|22;8|6.5;5.6|5.8;2
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Har du lært noe??;27;9;1.5;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, jeg har lært veldig mye.|Ja jeg larte myie.|Ja, jeg lært nue.;19|22;16.5|12.9;3|2.7;1
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Jeg tror at hvis du blir snill, så skal du gå på en tur. Men du må være snill og lydig!;27;11;8;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Det varer jeg!|Jeg skal det vare!|Det skal jeg være!;19|22;22|16.3;1.7|1.9;3']);

        $lastTaskChap3 = DB::table('tasks')->insertGetId(['name'=>'Zakończenie zadań trzeciego tygodnia', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$Chapter3Id, 'is_forced'=>1, 'parent_task_id' => $task4Id, 'content' => 'Powrót do mapy
Sam słyszałeś, wkrótce wyruszysz na wycieczkę :) Za dwa tygodnie poznasz całkiem nowe miasto. Kontynuuj naukę, a gra przyniesie Ci jeszcze wiele radości!']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
