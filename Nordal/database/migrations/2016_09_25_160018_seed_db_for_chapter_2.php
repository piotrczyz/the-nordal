<?php

use Illuminate\Database\Migrations\Migration;

class SeedDbForChapter2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * Changes
         */
        DB::table('maps')->where('name', "nordal")->update(['height' => 854, 'width'=>1272]);
        DB::table('maps')->where('name', "zibur")->update(['height' => 700, 'width'=>1337, 'url' => 'img/maps/zibur.png']);

        DB::table('pins')->update(['height' => 150]);
        DB::table('pins')->where('name', "Osada Zibur")
            ->update(['height' => 100, 'x'=>350, 'y'=>600]);

        $bot1Id = \Nordal\Models\Pin::where('name', 'Karczmarz')->firstOrFail()->id;
        $bot2Id = \Nordal\Models\Pin::where('name', 'Ivar')->firstOrFail()->id;
        $bot3Id = \Nordal\Models\Pin::where('name', 'Heidi')->firstOrFail()->id;
        $bot4Id = \Nordal\Models\Pin::where('name', 'Sigve')->firstOrFail()->id;

        /*
         * Dodawanie zadań
         */

        $fableTaskTypeId = Nordal\Models\TaskType::where('param1', '=', 'fable')->firstOrFail()->id;
        $conversationTaskTypeId = Nordal\Models\TaskType::where('param1', '=', 'conversation')->firstOrFail()->id;
        $Chapter2Id = Nordal\Models\Chapter::where('name', '=', 'Etap 2')->firstOrFail()->id;

        $lastTaskId = DB::table('tasks')->orderBy('id', 'desc')->first()->id;

        $fableId = DB::table('tasks')->insertGetId(['name'=>'Szkoła', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$Chapter2Id, 'is_forced'=>1, 'parent_task_id' => $lastTaskId, 'content' => 'Pokaż mapę
<p>Tak, to już dzisiaj jest ten dzień! Pierwszy dzień szkolenia! W końcu rozpoczniesz naukę szermierki, nawigacji oraz wielu innych rzeczy niezbędnych do wyruszenia na wyprawę. Zanim jednak ten wielki dzień się rozpocznie musisz zjeść porządne śniadanie i nabrać sił. Porozmawiaj z Heidi która razem z tobą pójdzie do szkoły.</p>
']);
        $task1 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Heidi','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter2Id, 'pin_id'=>$bot3Id, 'parent_task_id' => $fableId, 'to_load_audio' => '8,9,13',
            'content' =>'Wiking;img/characters/thumbs/[[user.character_number]].png;Player;God dag, Heidi!|Godag, Heidi!|Go dag, Heidi!;8|9;1.1|0.75;1.2|1;1
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Hei!;13;0.8;1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg å spise frokost!|Jeg spiser frokost.|Jeg spise frokost.;8|9;3.2|2.35;1.5|1.5;2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Du vil noe?|Du har lyst på noe?|Har du lyst på noe?;8|9;5.4|3.9;1.5|0.5;3
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Ja, takk.;13;2.25;0.9
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Vil kaffe eller te?|Vil du ha kaffe eller te?|Du vil kaffe eller te?;8|9;8.2|4.8;1.9|1.9;2
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Te, takk. Jeg drikker ikke kaffe.;13;4;2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ok. Her har du et brød, syltetei, szinke, ost, tomat…|Ok. Har her du et brø, siltetøy, sjinke, ust, tumat…|Ok. Her har du et brød, syltetøy, skinke, ost, tomat…;8|9;11.3|7.5;7.2|5.6;3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hva liker du best?|Hva like du mest?|Hva du liker best?;8|9;19|13.4;1.6|1.1;1
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Syltetøy, takk.;13;6.9;1.4
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Vær så god!|Vær så snill!|Væ så gu!;8|9;21.45|15.3;1.2|1.0;1']);
        $task2 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Ivarem','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter2Id, 'pin_id'=>$bot2Id, 'parent_task_id' => $task1, 'to_load_audio' => '8,10,14',
            'content' =>'Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Hei!;14;1.4;0.9
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Godag!|Gu dag!|God dag!;8|10;25.2|0.7;1|0.6;3
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Går dere på skole?;14;4.2;2.2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja. Vi tar buss. Hvor går du?|Ja. Vi går buss. Hvor går du?|Ja. Vi å ta buss. Hvor går du?;8|10;29.2|2;4|1.5;1
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Jeg går på jobb. Jeg jobber veldig mye.;14;8.6;5.1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Slapp av om lørda!|Slapa på lordag!|Slapp av på lørdag!;8|10;35.25|4;1.5|1.3;3
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Jeg skal slappe av i kveld. Ha en fin dag!;14;16.15;5
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Takk, i like måte!|Tak, i likemåte.|Tak, du ogso.;8|10;38.85|6.35;2|1.6;1']);
        $task3 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Karczmarzem','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter2Id, 'pin_id'=>$bot1Id, 'parent_task_id' => $task2, 'to_load_audio' => '8,11,15',
            'content' =>'Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Hei!;15;0.9;0.7;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;God dag!|Gu dag!|Godagg!;8|11;43.6|1;0.8|0.5;1
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Hvordan går det med deg?;15;2.3;1.4;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Bare bra, takk.|Jeg går bra, takk.|Bra, tak.;8|11;46.1|2.4;1.8|1.2;1
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Har du fri i dag?;15;5.2;1.5;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, men jeg mo handle å lage mat til min bestemur.|Ja, men jag må handele og lage matt til bestemor min.|Ja, men jeg må handle og lage mat til min bestemor.;8|11;49.6|4.45;4.7|3.2;3
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Du er veldig snill.;15;7.7;1.7;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, jeg ar.|Ja, jeg liker å hjelpe.|Ja, jeg like to hjelp.;8|11;55.4|8.8;2.5|1.9;2
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Lykke til!;15;10.3;1;
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Takk! Hade!|Tak! Ha de!|Takk! Ha det!;8|11;59|11.3;1.9|1.4;3']);
        $task4 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Sigve','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter2Id, 'pin_id'=>$bot4Id, 'parent_task_id' => $task3, 'to_load_audio' => '8,12,16',
            'content' =>'Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;God dag!;16;1.3;1.5
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Haj!|Hei!|Gudag!;8|12;62.8|0.7;0.7|0.5;2
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Leser du en bok?;16;4.7;1.9
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja. Liker du å lese?|Ja. Like du lese?|Ja. Jeg like lese.;8|12;64.2|2;2.7|1.7;1
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Jeg liker å høre på musikk best. Nå må jeg gå. Jeg må være hjemme om ti minutter.;16;8.7;8
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hade!|Var kosli å se dag!|Det var koselig å se deg!;8|12;68.1|4.7;1.5|1.4;3']);

        DB::table('tasks')->insert(['name'=>'Zakończenie zadań z drugiego tygodnia', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$Chapter2Id, 'is_forced'=>1, 'parent_task_id' => $task4, 'content' => 'Powrót do mapy
I tak kończy się męczący dzień w szkole. Ale nie było wcale tak źle :)']);
        /*
         * Dodawanie zadań end
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
