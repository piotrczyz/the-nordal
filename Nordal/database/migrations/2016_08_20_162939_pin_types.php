<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PinTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pin_types', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('name');
	        
        });

	    DB::table('pin_types')->insert(['name' => 'city']);
        DB::table('pin_types')->insert(['name' => 'bot']);

	    Schema::table('pins', function (Blueprint $table) {
		    $table->integer('pin_type_id')->unsigned()->nullable();
		    $table->foreign('pin_type_id')->references('id')->on('pin_types')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pin_types');

	    Schema::table('pins', function (Blueprint $table) {
	    	$table->dropForeign('pins_pin_type_id_foreign');
	    	$table->dropColumn('pin_type_id');
	    });
    }
}
