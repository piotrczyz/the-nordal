<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AvatarForUserAndTaskTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar');
        });
	    
	    $taskType = \Nordal\Models\TaskType::where('param1','conservation')->get();
	    if ($taskType->isEmpty()){
	    	$taskType = new \Nordal\Models\TaskType();
		    $taskType->Name = 'Rozmowa';
		    $taskType->Param1 = 'conversation';
		    $taskType->save();
	    }
	    
	    
	    $taskType = \Nordal\Models\TaskType::where('param1','message')->get();
	    if ($taskType->isEmpty()){
		    $taskType = new \Nordal\Models\TaskType();
		    $taskType->Name = 'Wiadomość';
		    $taskType->Param1 = 'message';
		    $taskType->save();
	    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->removeColumn('avatar');
        });
    }
}
