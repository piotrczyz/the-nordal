<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToLoadAudioToTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function(Blueprint $table) {
            $table->text('to_load_audio')->nullable();
        });
        DB::table('tasks')
            ->where('name', 'Rozmowa z Karczmarzem')
            ->update(['to_load_audio' => "2,3,4"]);
        DB::table('tasks')
            ->where('name', 'Rozmowa z Ivarem')
            ->update(['to_load_audio' => "2,3,5"]);
        DB::table('tasks')
            ->where('name', 'Rozmowa z Heidi')
            ->update(['to_load_audio' => "2,3,6"]);
        DB::table('tasks')
            ->where('name', 'Rozmowa ze starcem Sigve')
            ->update(['to_load_audio' => "2,3,7"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn('to_load_audio');
        });
    }
}
