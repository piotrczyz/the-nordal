<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Glossary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('chapters', function (Blueprint $table){
		    $table->increments('id');

		    $table->char('name');
		    $table->integer('week');
	    });

	    Schema::create('files', function (Blueprint $table){
		    $table->increments('id');
		    $table->timestamps();
		    
		    $table->char('name');
		    $table->string('path', 1025);
	    });
	    
	    Schema::create('glossaries', function (Blueprint $table){
		    $table->increments('id');

		    $table->char('pl');
		    $table->char('no');
		    
		    $table->integer('file_id')->unsigned();
		    $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
		    
		    $table->integer('chapter_id')->unsigned();
		    $table->foreign('chapter_id')->references('id')->on('chapters')->onDelete('cascade');		    
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('glossaries');
	    Schema::drop('files');
	    Schema::drop('chapters');

    }
}
