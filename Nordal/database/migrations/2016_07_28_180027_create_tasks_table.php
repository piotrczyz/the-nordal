<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('tasks_types', function (Blueprint $table) {
		    $table->increments('id');

		    $table->char('name');
		    $table->text('param1');
	    });
	    
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
	        
	        $table->char('name');
	        $table->text('description')->nullable();
	        $table->integer('exp')->default(0);
	        $table->boolean('repeatable')->default(true);
	        
	        $table->integer('type_id')->unsigned();
	        $table->foreign('type_id')->references('id')->on('tasks_types');
	        
	        $table->integer('parent_task_id')->unsigned()->nullable();
	        $table->foreign('parent_task_id')->references('id')->on('tasks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('tasks', function (Blueprint $table){
    		$table->dropForeign('tasks_type_id_foreign');
    		$table->dropForeign('tasks_parent_task_id_foreign');
	    });
	    Schema::drop('tasks_types');
	    Schema::drop('tasks');
    }
}
