<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Nordal\Models\Pin;
use Nordal\Models\Task;

class ChangeRelationBetweenTasksAndPins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	if(!Schema::hasColumn('tasks','pin_id')) {
    		//add pin_id to a tasks table
		    Schema::table('tasks', function (Blueprint $table) {
			    $table->integer('pin_id')->unsigned()->nullable();
			    $table->foreign('pin_id')->references('id')->on('pins')->onDelete('set null');
		    });
		    
		    //switch relation
		    foreach (Pin::get() as $pin){
			    $task = Task::find($pin->Task->Id);
			    $task->PinId = $pin->Id;
			    $task->save();
		    }

		    Schema::disableForeignKeyConstraints();
		    Schema::table('pins', function (Blueprint $table) {
			    $table->dropForeign('pins_task_id_foreign')->unsigned()->nullable();
			    $table->dropColumn('task_id');
		    });
		    Schema::enableForeignKeyConstraints();

	    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
