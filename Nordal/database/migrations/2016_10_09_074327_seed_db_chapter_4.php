<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedDbChapter4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    $Chapter4Id = DB::table('chapters')->insertGetId(['name' => 'Etap 4', 'date_start' => '2016-10-08 19:00:00']);

	    $fableTaskTypeId = DB::table('tasks_types')->where('param1', 'fable')->value('id');
	    $sentencesTaskTypeId = DB::table('tasks_types')->where('param1', 'sentences')->value('id');
	    $botIvarId = DB::table('pins')->where('name', 'Ivar')->value('id');
	    $botHeidiId = DB::table('pins')->where('name', 'Heidi')->value('id');
	    $botSigveId = DB::table('pins')->where('name', 'Sigve')->value('id');

	    $lastTaskId = DB::table('tasks')->orderBy('id', 'desc')->first()->id;

	    $fableId = DB::table('tasks')->insertGetId(['name'=>'Czas na wiedzę', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$Chapter4Id, 'is_forced'=>1, 'parent_task_id' => $lastTaskId, 'content' => 'Pokaż mapę
<p>W tym tygodniu poćwiczymy układanie słówek po norwesku :)</p>
']);
	    $task1Id = DB::table('tasks')->insertGetId(['name'=>'Zdania oznajmujące z Heidi','exp'=>10,'type_id'=>$sentencesTaskTypeId,
		    'chapter_id'=>$Chapter4Id, 'pin_id'=>$botHeidiId, 'parent_task_id' => $fableId,
		    'content' =>'<h2>Cześć</h2><p>Ze mną poćwiczysz tworzenie zdań oznajmujących, do dzieła!</p>
Tata siedzi w salonie.;Pappa|sitter|i|stua|å sitte|på|papa;Pappa|sitter|i|stua
Nelly cieszy się na konferencjęletnią w Brunstad.;Nelly|gleder|seg|til|sommerstevnet|på|Brunstad|gledet|smile;Nelly|gleder|seg|til|sommerstevnet|på|Brunstad
Oni pracują w kuchni.;De|jobber|på|kjøkkenet|dem|jobb|kjelleren;De|jobber|på|kjøkkenet
Jego samochód jest biały.;Hans|bil|er|hvit|sykkel|vit|ham;Hans|bil|er|hvit
Jej brat jest spokojny i cierpliwy.;Hennes|bror|er|rolig|og|tålmodig|være|henne|i;Hennes|bror|er|rolig|og|tålmodig']);

        $task2Id = DB::table('tasks')->insertGetId(['name'=>'Zdania pytające z Ivarem','exp'=>10,'type_id'=>$sentencesTaskTypeId,
            'chapter_id'=>$Chapter4Id, 'pin_id'=>$botIvarId, 'parent_task_id' => $task1Id,
            'content' =>'<h2>Cześć</h2><p>Ze mną potrenujesz budowanie pytań, do roboty!</p>
Gdzie jest twój rower?;Hvor|er|din|sykkel;Hvor|er|din|sykkel
Masz ochotę iść na spacer?;Har|du|lyst|til|å|gå|på|tur;Har|du|lyst|til|å|gå|på|tur
Mogę dostać kanapkę z serem i szynką?;Kan|jeg|få|brød|med|ost|og|skinke;Kan|jeg|få|brød|med|ost|og|skinke
Boisz się?;Er|du|redd;Er|du|redd
Lubisz rysować?;Liker|du|å|tegne;Liker|du|å|tegne']);

        $task3Id = DB::table('tasks')->insertGetId(['name'=>'Przeczenia z Sigve','exp'=>10,'type_id'=>$sentencesTaskTypeId,
            'chapter_id'=>$Chapter4Id, 'pin_id'=>$botSigveId, 'parent_task_id' => $task2Id,
            'content' =>'<h2>Witaj</h2><p>Zobaczymy czy potrafisz już przeczyć!</p>
  On nie jest stary.;Han|er|ikke|gammel;Han|er|ikke|gammel
  Ona nigdy nie jest smutna.;Hun|er|aldri|trist;Hun|er|aldri|trist
  Elise nie lubi śpiewać.;Elise|liker|ikke|å|synge;Elise|liker|ikke|å|synge
  Tom nie idzie jutro do szkoły.;Tom|går|ikke|på|skole|i|morgen;Tom|går|ikke|på|skole|i|morgen
  Mój kuzyn nie biega często.;Min|fetter|jogger|ikke|ofte;Min|fetter|jogger|ikke|ofte']);

DB::table('tasks')->insert(['name'=>'Zakończenie zadań czwartego tygodnia', 
            'type_id'=>$fableTaskTypeId, 'chapter_id'=>$Chapter4Id, 'is_forced'=>1, 
            'parent_task_id' => $task3Id, 'content' => 'Powrót do mapy
Ukończyłeś wszystkie zadania na ten tydzień! Jeśli nie udało ci się za pierwszym razem ułożyć wszystkich zdań poprawnie, nie martw się! Możesz spróbować rozwiązać zadanie ponownie.']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
