<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentMapIdToMaps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('maps', function(Blueprint $table){
            $table->integer('parent_map_id')->unsigned()->nullable();
            $table->foreign('parent_map_id')->references('id')->on('maps')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('maps', function (Blueprint $table) {
            $table->dropForeign('maps_parent_map_id_foreign');
            $table->dropColumn('parent_map_id');
        });
    }
}
