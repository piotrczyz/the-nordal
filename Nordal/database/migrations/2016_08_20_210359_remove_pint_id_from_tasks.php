<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemovePintIdFromTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    Schema::table('tasks', function (Blueprint $table) {
		    $table->dropForeign('tasks_pin_id_foreign');
		    $table->dropColumn('pin_id');
	    });
    	
        Schema::table('pins', function (Blueprint $table) {
	        $table->integer('task_id')->unsigned()->nullable();
	        $table->foreign('task_id')->references('id')->on('tasks')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('tasks', function (Blueprint $table) {
		    $table->integer('pin_id')->unsigned()->nullable();
		    $table->foreign('pin_id')->references('id')->on('pins')->onDelete('set null');
		    
	    });
        Schema::table('pins', function (Blueprint $table) {
	        $table->dropForeign('pins_task_id_foreign');
	        $table->dropColumn('task_id');
        });
    }
}
