<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateStartColumnForChapters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chapters', function (Blueprint $table) {
            $table->dateTime('date_start')->nullable();
        });
        DB::update('update chapters set date_start = ? where name = ?', ['2016-09-16 19:00:00', 'Etap 1']);
        DB::update('update chapters set date_start = ? where name = ?', ['2016-09-25 19:00:00', 'Etap 2']);
        DB::update('update chapters set date_start = ? where name = ?', ['2016-10-02 19:00:00', 'Etap 3']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chapters', function (Blueprint $table) {
            $table->dropColumn('date_start');
        });
    }
}
