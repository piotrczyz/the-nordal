<?php

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Nordal\Models\Action;

class ChangeExpToFishes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fishing = false;
        $actions = Action::get();
        foreach ($actions as $action){
            /** @var $action Action */

            if (strtotime($action->CreatedDateTime) === strtotime($action->UpdatedDateTime)) {

                $action->Exp = floor($action->Exp / 5);
                $action->UpdatedDateTime = Carbon::now();
                $action->save();
                $fishing = true;
            }
        }

        if ($fishing){
            $tasks = \Nordal\Models\Task::get();
            foreach ($tasks as $task){
                /** @var \Nordal\Models\Task $task */
                $task->Exp = floor($task->Exp/5);
                $task->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
