<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('maps', function (Blueprint $table) {
		    $table->increments('id');
		    $table->char('name');
	    });
	    
	    Schema::create('pins', function (Blueprint $table) {
		    $table->increments('id');
		    
		    $table->char('name')->nullable();
		    $table->double('x');
		    $table->double('y');
		    
		    $table->integer('map_id')->unsigned()->nullable();
		    $table->foreign('map_id')->references('id')->on('maps')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('pins');
	    Schema::drop('maps');
    }
}
