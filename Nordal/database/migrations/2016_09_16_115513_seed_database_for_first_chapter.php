<?php

use Illuminate\Database\Migrations\Migration;

class SeedDatabaseForFirstChapter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('chapters')->insert(['name' => 'Etap 1', 'week' => '38']);
        DB::table('chapters')->insert(['name' => 'Etap 2', 'week' => '39']);
        DB::table('chapters')->insert(['name' => 'Etap 3', 'week' => '40']);

        $mapNordalId = DB::table('maps')->insertGetId(['name'=>'nordal', 'url'=>'img/maps/nordal.png', 'description'=>'Mapa krainy Nordal', 'height'=>'854', 'width'=>'1272']);
        $mapZiburId = DB::table('maps')->insertGetId(['name'=>'zibur',  'url'=>'img/maps/zibur.png',  'description'=>'Osada Zibur',        'height'=>'700', 'width'=>'1337', 'parent_map_id' => $mapNordalId]);

        $cityTypeId = \Nordal\Models\PinType::where('name', 'city')->firstOrFail()->id;
	    if (($bot = \Nordal\Models\PinType::where('name', 'bot')->first())){
		    $botTypeId = $bot->id;
	    } else {
	    	$bot = new \Nordal\Models\PinType();
		    $bot->Name = 'bot';
		    $bot->save();
		    $botTypeId = \Nordal\Models\PinType::where('name', 'bot')->first()->id;
	    }

        DB::table('pins')->insert(['name'=>'Osada Zibur','x'=>350,'y'=>600, 'height'=>100, 'map_id'=>$mapNordalId, 'pin_type_id'=>$cityTypeId, 'url'=>'img/map_objects/zibur_small.png', 'destination_map_id'=>$mapZiburId]);
        $bot1Id = DB::table('pins')->insertGetId(['name'=>'Karczmarz','x'=>400,'y'=>60, 'height'=>150, 'map_id'=>$mapZiburId, 'pin_type_id'=>$botTypeId, 'url'=>'img/map_objects/Zibur/bot1.png']);
        $bot2Id = DB::table('pins')->insertGetId(['name'=>'Ivar','x'=>150,'y'=>200, 'height'=>150, 'map_id'=>$mapZiburId, 'pin_type_id'=>$botTypeId, 'url'=>'img/map_objects/Zibur/bot2.png']);
        $bot3Id = DB::table('pins')->insertGetId(['name'=>'Heidi','x'=>300,'y'=>300, 'height'=>150, 'map_id'=>$mapZiburId, 'pin_type_id'=>$botTypeId, 'url'=>'img/map_objects/Zibur/bot3.png']);
        $bot4Id = DB::table('pins')->insertGetId(['name'=>'Sigve','x'=>700,'y'=>270, 'height'=>150, 'map_id'=>$mapZiburId, 'pin_type_id'=>$botTypeId, 'url'=>'img/map_objects/Zibur/bot4.png']);

        /*
         * Dodawanie zadań
         */

        $fableTaskTypeId = Nordal\Models\TaskType::where('param1', '=', 'fable')->firstOrFail()->id;
        $choose_characterTaskTypeId = Nordal\Models\TaskType::where('param1', '=', 'choose_character')->firstOrFail()->id;
        $conversationTaskTypeId = Nordal\Models\TaskType::where('param1', '=', 'conversation')->firstOrFail()->id;
        $firstChapterId = Nordal\Models\Chapter::where('name', '=', 'Etap 1')->firstOrFail()->id;

        DB::table('tasks')->insert(['name'=>'Kraina Nordalu - Wstęp', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$firstChapterId, 'is_forced'=>1, 'content' => 'Chcę!
<p>Na  dalekiej, zimnej północy, w dzikiej, porośniętej dziewiczymi lasami krainie zwanej <i>Nordalem</i>, porozrzucane są niewielkie, malownicze osady wikingów. Ich mieszkańcy to lud twardy, nieustępliwy i odważny. Nie straszne im dalekie podróże, ani wielkie podboje.</p>
<p>Legenda głosi, że jeden z nich, wojownik o imieniu <i>Wranga</i>, dotarł dalej niż inni i odnalazł ukryty przed wiekami skarb. Jednak jego statek zatonął, a o Wrandze zaginął wszelki słuch.</p>
<p>Każdy wikiński chłopiec i dziewczyna marzą, by pójść w ślady osławionego bohatera. Ale w wyprawach udział brać mogą tylko najsilniejsi i najbardziej zahartowani wojownicy, dlatego każdy z młodych śmiałków musi dowieść, że zasługuje na miejsce na statku wikingów płynącym w nieznane.</p>
<strong>Czy chcesz zostać wikińskim adeptem?</strong>']);
        DB::table('tasks')->insert(['name'=>'Wybór postaci', 'type_id'=>$choose_characterTaskTypeId, 'chapter_id'=>$firstChapterId, 'is_forced'=>1]);
        $task1 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Karczmarzem','exp'=>10,'type_id'=>$conversationTaskTypeId, 'chapter_id'=>$firstChapterId, 'pin_id'=>$bot1Id,
            'content' =>'Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hei!|Hai!|Hopsa!;2|3;3.5|2.4;1|1;1
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Hei!;4;1;1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hvis hetter du?|Var hete du?|Hva heter du?;2|3;5.1|3.8;2|1.3;3
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Jeg heter Telan.;4;2;2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ok, takk. Hade!|Ok, takk. Til gensyn!|Ok, takk. Ha det!;2|3;7.6|5.9;3|2.6;3
Karczmarz;img/map_objects/Zibur/avatars/bot1.png;Bot;Ha det!;4;4.0;1.0']);
        $task2 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Ivarem','exp'=>10,'type_id'=>$conversationTaskTypeId, 'chapter_id'=>$firstChapterId, 'pin_id'=>$bot2Id, 'parent_task_id' => $task1,
            'content' =>'Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hei! Hvordan går hos deg?|Hei! Hvordan går det med deg?|Hei hva høres hos deg?;2|3;11.75|9.1;3.5|2.7;2
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Bare bra, takk.;5;1;1.9
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Heter du Sigve?|Heter du deg Sigve?|Hetter jeg Sigve?;2|3;16.1|12.05;1.5|1.5;1
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Hei! Nei, jeg heter ikke Sigve. Jeg heter Ivar.;5;3.4;3.2
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Hvor kommer Sigve fra?;5;7.3;1.7
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Nei vet jeg.|Jeg vet ikke.|Jeg ikke vet det.;2|3;19.055|14.2;1.225|1.4;2
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Hvem er Sigve?;5;9.7;1.25
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Er han mester.|Han ar master.|Han er en mester.;2|3;21.225|16.58;1.675|1.5;3
Ivar;img/map_objects/Zibur/avatars/bot2.png;Bot;Takk. Ha det!;5;11.5;1.25']);
        $task3 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Heidi','exp'=>10,'type_id'=>$conversationTaskTypeId, 'chapter_id'=>$firstChapterId, 'pin_id'=>$bot3Id, 'parent_task_id' => $task2,
            'content' =>'Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hi! Snakke du norsk?|Hei! Du snakker norsk?|Hei! Snakker du norsk?;2|3;24.15|18.925;3.2|2.35;3
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Ja, jeg snakker norsk.;6;0.75;2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Sigve bor her?|Bor Sigve her?|Her bor Sigve?;2|3;28.55|21.9;1.7|1.5;2
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Nei. Er han ung?;6;2.9;1.75
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Nei, han er gammel.|Nei, han er ung.|Ja, han er gamle.;2|3;31.15|24.35;2.35|2;1
Heidi;img/map_objects/Zibur/avatars/bot3.png;Bot;Gammel Sigve er min bestefar. Han bor her, i Zibur.;6;5.15;4.2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Takk!|Taket!|Tanks!;2|3;37.41|29.47;0.81|0.75;1']);
        $task4 = DB::table('tasks')->insertGetId(['name'=>'Rozmowa ze starcem Sigve','exp'=>10,'type_id'=>$conversationTaskTypeId, 'chapter_id'=>$firstChapterId, 'pin_id'=>$bot4Id, 'parent_task_id' => $task3,
            'content' =>'Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hei! Du er Sigve from Zibur?|Hei! Er du Sigve fra Zibur?|Hei! Var Sigve fra Zibur?;2|3;39.225|30.95;3.25|3;2
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Hei! Ja, jeg er Sigve. Snakker du norsk?;7;1;3.6
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, jeg snakker litt norsk.|Nei, jeg snakker norsk.|Ja, jeg snakker ikke norsk.;2|3;43.3|34.575;3|2.075;1
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Hvor kommer du fra?;7;5.75;1.3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Du kommer fra Nordal.|Jeg kommer fra Nordal.|Jeg kommer i Nordal.;2|3;49.7|40;2.2|1.8;2
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Hvor gammel er du?;7;8.45;1.2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg har 15 år gammel. Er du en mester?|Jeg har 15. Er du en mester?|Jeg er 15 år gammel. Er du en mester?;2|3;67.1|52.9;4.5|3.25;3
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Ja, jeg er en mester.;7;11.25;2.3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Koselig å se deg.|Koselig at si deg.|Koselig å si meg.;2|3;72.7|56.6;1.6|1.45;1']);

        DB::table('tasks')->insert(['name'=>'Zakończenie zadań z pierwszego tygodnia', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$firstChapterId, 'is_forced'=>1, 'parent_task_id' => $task4, 'content' => 'Powrót do mapy
Brawo! Udało ci się odnaleźć mistrza Sigve! Odtąd będzie pomagał ci w szkoleniu się na najprawdziwszego wikinga. Z jego pomocą odnajdziesz skarb!']);
        /*
         * Dodawanie zadań end
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
