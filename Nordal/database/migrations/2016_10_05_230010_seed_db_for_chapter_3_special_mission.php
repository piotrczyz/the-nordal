<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedDbForChapter3SpecialMission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Najpierw standardowe zadania
        $fableTaskTypeId = DB::table('tasks_types')->where('param1', 'fable')->value('id');
        $conversationTaskTypeId = DB::table('tasks_types')->where('param1', 'conversation')->value('id');
        $Chapter3Id = DB::table('chapters')->where('name', 'Etap 3')->value('id');

        $lastTaskId = DB::table('tasks')->orderBy('id', 'desc')->first()->id;

        // Zadania z misji specjalnej - Powstań i walcz

        $mapNordalId = DB::table('maps')->where('name', 'nordal')->value('id');
        $mapCaveId = DB::table('maps')->insertGetId(['name'=>'cave',  'url'=>'img/maps/cave.png',  'description'=>'Tajemnicza jaskinia',
            'height'=>'700', 'width'=>'1273', 'parent_map_id'=>$mapNordalId, 'audio_id'=>37]);

        $cityPinTypeId = DB::table('pin_types')->where('name', 'city')->value('id');
        DB::table('pins')->insertGetId(['name'=>'Jaskinia','x'=>700,'y'=>440, 'height'=>75, 'map_id'=>$mapNordalId, 'pin_type_id'=>$cityPinTypeId, 'url'=>'img/map_objects/cave_small.png', 'destination_map_id'=>$mapCaveId]);

        $botPinTypeId = DB::table('pin_types')->where('name', 'bot')->value('id');
        $pinGoblin1Id = DB::table('pins')->insertGetId(['name'=>'Goblin 1','x'=>175,'y'=>417, 'height'=>200, 'map_id'=>$mapCaveId, 'pin_type_id'=>$botPinTypeId, 'url'=>'img/map_objects/cave/goblin1.png']);
        $pinGoblin2Id = DB::table('pins')->insertGetId(['name'=>'Goblin 2','x'=>956,'y'=>406, 'height'=>200, 'map_id'=>$mapCaveId, 'pin_type_id'=>$botPinTypeId, 'url'=>'img/map_objects/cave/goblin2.png']);
        $pinGoblin3Id = DB::table('pins')->insertGetId(['name'=>'Goblin 3','x'=>530,'y'=>471, 'height'=>200, 'map_id'=>$mapCaveId, 'pin_type_id'=>$botPinTypeId, 'url'=>'img/map_objects/cave/goblin3.png']);

        $fableStandUpAndFightId = DB::table('tasks')->insertGetId(['name'=>'Powstań i walcz!', 'type_id'=>$fableTaskTypeId, 'chapter_id'=>$Chapter3Id, 'is_forced'=>1, 'parent_task_id' => $lastTaskId, 'content' => 'Podejmij wyzwanie!
<div style="
    margin-bottom: 10px;
    background: black;
    text-align: center;
"><img src="img/missions/other/piw_black.png" alt="Powstań i walcz baner"></div>
<p>"Straszne wieści!" zdyszany starzec Sigve krzyczy z oddali. "Chodzi o mojego przyjaciela Olafa" mówi dalej, z trudem łapiąc powietrze. "Od trzech dni nie wrócił do domu i nie wiem co się z nim stało. Możesz go odnaleźć i pomóc mu wrócić do Zibur?" pyta starzec. Gdy mieszkańcy osady są w niebezpieczeństwie, każdy wiking porzuca swoje zajęcie i rusza do akcji! Teraz i ty możesz dowieść swej walczeności. Jesteś gotów pomóc Sigve i odnaleźć jego przyjaciela?</p>
']);
        $goblinTask1Id = DB::table('tasks')->insertGetId(['name'=>'Goblin 1','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'pin_id'=>$pinGoblin1Id, 'parent_task_id' => $fableStandUpAndFightId, 'to_load_audio' => '28,31,34',
            'content' =>'Goblin 1;img/map_objects/cave/avatars/goblin1.png;Bot;Hahaha, hvem er du?;34;0.8;2.4
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg er en viking og jeg er ikke redd for dere!|Jeg vil spise|Eeeeh… Hvor er min mamma?;28|31;0.3|0.8;4.2|4.1;1
Goblin 1;img/map_objects/cave/avatars/goblin1.png;Bot;Men vi er veldig sterke… Ikke kjemp mot oss!;34;4;4.5
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg er ikke redd, reis deg til strid!|Jeg heter Ivar|Jeg er redd, reis til strid!;28|31;4.5|5.5;3.4|3;1
Goblin 1;img/map_objects/cave/avatars/goblin1.png;Bot;Bum;34;8.5;1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Bam;28|31;8|8.7;2|1.3
Goblin 1;img/map_objects/cave/avatars/goblin1.png;Bot;Zzzzz….;34;10.1;1.3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Hurra, jeg heter seier!|Hurra, seier!|Jeg seier!;28|31;10.2|10.6;2.7|2;2']);
        $goblinTask2Id = DB::table('tasks')->insertGetId(['name'=>'Goblin 2','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'pin_id'=>$pinGoblin2Id, 'parent_task_id' => $goblinTask1Id, 'to_load_audio' => '29,32,35',
            'content' =>'Goblin 2;img/map_objects/cave/avatars/goblin1.png;Bot;Hvor kommer du fra?;35;0.8;1.6
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg kommer Zibur.|Jeg fra Zibur kommer.|Jeg kommer fra Zibur.;29|32;0.2|0.6;2|2;3
Goblin 2;img/map_objects/cave/avatars/goblin1.png;Bot;Ååå, Olaf kommer også fra Zibur.;35;3.5;3.1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Snakker du Olaf?|Hvor er Olaf?!|Jeg heter Olaf?!;29|32;2.6|3;1.2|1.6;2
Goblin 2;img/map_objects/cave/avatars/goblin1.png;Bot;Haha, vi har lyst til å spise ham.;35;7.3;4.4
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Det skal ikke!|Det skal skje!|Det skal ikke skje!;29|32;4.6|4.9;1.8|1.7;3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Bang bang!;29|32;6.6|7.2;2|1.4']);
        $goblinTask3Id = DB::table('tasks')->insertGetId(['name'=>'Goblin 3','exp'=>10,'type_id'=>$conversationTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'pin_id'=>$pinGoblin3Id, 'parent_task_id' => $goblinTask2Id, 'to_load_audio' => '30,33,36',
            'content' =>'Goblin 3;img/map_objects/cave/avatars/goblin3.png;Bot;Uff, du er veldig sterk. Er du ikke redd for oss?;36;0.2;5
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg er aldri redd for noen.|Jeg aldri redd for noen.|Jeg redd noen.;30|33;1.2|1;2.4|2.6;1
Goblin 3;img/map_objects/cave/avatars/goblin3.png;Bot;Ok, jeg skal banke deg.;36;5.4;2.7
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Det kjedelig.|Det er siedlig.|Det er kjedelig.;30|33;7|8;2|1.5;3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Du må møte dine venner. Jeg slo dem.|Du mote venner. Jeg slo dem.|Du må motte dine venner. Jeg slo dem.;30|33;11|11;4|3.4;1
Goblin 3;img/map_objects/cave/avatars/goblin3.png;Bot;Hmm…;36;8.4;1
Goblin 3;img/map_objects/cave/avatars/goblin3.png;Bot;Oj, faktisk. Kanskje vi kan gå og spille fotball?;36;9.9;4.8
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, gjerne|Ja, jerne|Ja, gerne;30|33;15.5|18.2;1.6|2.2;1']);

        $lastTaskStandUpAndFightId = DB::table('tasks')->insertGetId(['name'=>'Zakończenie misji Powstań i walcz!', 'type_id'=>$fableTaskTypeId,
            'chapter_id'=>$Chapter3Id, 'is_forced'=>1, 'parent_task_id' => $goblinTask3Id, 'content' => 'Powrót do mapy
<div style="
    margin-bottom: 10px;
    background: black;
    text-align: center;
"><img src="img/missions/other/piw_black.png" alt="Powstań i walcz baner"></div>
<p>Odnalazłeś uwięzionego Olafa i tym samym ukończyłeś misję <b>Powstań i walcz</b>. Gratulacje i wielkie dzięki w imieniu osady Zibur.</p>']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
