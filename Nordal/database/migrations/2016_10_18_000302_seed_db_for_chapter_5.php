<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedDbForChapter5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    $Chapter5Id = DB::table('chapters')->insertGetId(['name' => 'Etap 5', 'date_start' => '2016-10-16 19:00:00']);

	    $mapNordalId = DB::table('maps')->where('name', 'nordal')->value('id');
	    $mapRensaId = DB::table('maps')->insertGetId(['name'=>'rensa',  'url'=>'img/maps/rensa.png',  'description'=>'Port Rensa',
		    'height'=>'942', 'width'=>'1337', 'parent_map_id'=>$mapNordalId, 'audio_id'=>42]);

	    $cityPinTypeId = DB::table('pin_types')->where('name', 'city')->value('id');
	    DB::table('pins')->insertGetId(['name'=>'Port Rensa','x'=>679,'y'=>545, 'height'=>100, 'map_id'=>$mapNordalId,
		    'pin_type_id'=>$cityPinTypeId, 'url'=>'img/map_objects/rensa_small.png', 'destination_map_id'=>$mapRensaId]);

	    $botPinTypeId = DB::table('pin_types')->where('name', 'bot')->value('id');
	    $botJarl = DB::table('pins')->insertGetId(['name'=>'Jarl Torben','x'=>500,'y'=>450, 'height'=>150, 'map_id'=>$mapRensaId,
		    'pin_type_id'=>$botPinTypeId, 'url'=>'img/map_objects/Rensa/jarl_torben.png']);
	    $botOlaf = DB::table('pins')->insertGetId(['name'=>'Olaf','x'=>1100,'y'=>380, 'height'=>150, 'map_id'=>$mapRensaId,
		    'pin_type_id'=>$botPinTypeId, 'url'=>'img/map_objects/Rensa/olaf.png']);
	    $botArnora = DB::table('pins')->insertGetId(['name'=>'Arnora','x'=>220,'y'=>360, 'height'=>150, 'map_id'=>$mapRensaId,
		    'pin_type_id'=>$botPinTypeId, 'url'=>'img/map_objects/Rensa/arnora.png']);
	    $botVestar = DB::table('pins')->insertGetId(['name'=>'Vestar','x'=>530,'y'=>600, 'height'=>180, 'map_id'=>$mapRensaId,
		    'pin_type_id'=>$botPinTypeId, 'url'=>'img/map_objects/Rensa/vestar.png']);
	    $botSigveId = DB::table('pins')->where('name', 'Sigve')->value('id');

	    $fableTaskTypeId = DB::table('tasks_types')->where('param1', 'fable')->value('id');
	    $sentencesTaskTypeId = DB::table('tasks_types')->where('param1', 'sentences')->value('id');
	    $conversationTaskTypeId = DB::table('tasks_types')->where('param1', 'conversation')->value('id');

	    $lastTaskId = DB::table('tasks')->orderBy('id', 'desc')->first()->id;

	    $fableId = DB::table('tasks')->insertGetId(['name'=>'Podróż do portu', 'type_id'=>$fableTaskTypeId,
		    'chapter_id'=>$Chapter5Id, 'is_forced'=>1, 'parent_task_id' => $lastTaskId, 'content' => 'Pokaż mapę
Po tylu tygodniach nadszedł ten długo wyczekiwany dzień: dzień podróży. Teraz pozostało już tylko dopasowanie ostatnich ubrań i kanapek na drogę i w końcu zacznie się ta przygoda. Pożegnaj się jeszcze z Sigve i wyrusz do miasta portowego – Rensy. ']);

	    $task1Id = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Sigve','exp'=>10,'type_id'=>$conversationTaskTypeId,
		    'chapter_id'=>$Chapter5Id, 'pin_id'=>$botSigveId, 'parent_task_id' => $fableId, 'to_load_audio' => '38,39,40',
		    'content' =>'Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Halo Sigve!|Halu Sigve!|Hallo Sigve!;38|39;0.7|1.1;1.3|1.2;3
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Hei! Endelig kom den store dagen!;40;0.6;3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja! Jeg glæder mig vældig!|Ja! Jeg gleder meg veldig!|Ja! Jeg er glede meg meget!;38|39;2.8|3.2;2.2|2.3;2
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Rensa er en stor by, så du skal ikke kjede deg.;40;4.9;3.7
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, det bliver spændende, mester.|Ja, det blir spennende, mester.|Ja, de blir spenende, mester.;38|39;6.1|6.5;2.2|2.8;2
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Olaf venter på deg der.;40;14.2;1.8
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg vet, mester. Jeg skal hjelpe ham når han skal trenge det.|Jeg veit, mester. Jeg ska hjælpe ham når en ska trenge de.|Jeg vet, mester. Jeg skal hjelper ham når han skal trenger det.;38|39;9.1|10.4;4.2|3.7;1
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Veldig bra. Du må også finne Sommerens Sverd (Miecz Lata) og bringe det til meg.;40;18;4.9
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Somerens Sverd? Ok, det skar jeg finde.|Sommerens Sverd? Ok, det skal jeg finner.|Sommerens Sverd? Ok, det skal jeg finne.;38|39;14.1|15;3.6|3.7;3
Sigve;img/map_objects/Zibur/avatars/bot4.png;Bot;Du må dra nå, for det blir for mørkt.;40;25;2.5
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ha det, mester. Takk for alt!|Hav det, mester. Tak for alt!|Haddet, mæstar. Takk for alt!;38|39;18.7|19.5;2.2|2.1;1']);

	    $task2Id = DB::table('tasks')->insertGetId(['name'=>'Rozmowa z Jarlem Torbenem','exp'=>10,'type_id'=>$conversationTaskTypeId,
		    'chapter_id'=>$Chapter5Id, 'pin_id'=>$botJarl, 'parent_task_id' => $task1Id, 'to_load_audio' => '38,39,41',
		    'content' =>'Jarl Torben;img/map_objects/Rensa/avatars/jarl_torben.png;Bot;Velkommen i byen min.;41;1.6;1.8
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Kuslerig at blive kent med dag.|Koselig å bli kjent med deg.|Koselig og blir kjent med deg.;38|39;22.35|25;1.4|1.9;2
Jarl Torben;img/map_objects/Rensa/avatars/jarl_torben.png;Bot;Hva skal du gjøre her?;41;5.4;1.3
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Jeg skal lære hvordan å bygge båt.|Jeg skal lærer hvurdan å byge båt.|Jeg skal lærer å hvordan bygger båt.;38|39;24.8|28;1.9|2.6;1
Jarl Torben;img/map_objects/Rensa/avatars/jarl_torben.png;Bot;Er du sliten?;41;8.6;1.1
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, det var en lang reise.|Ja, de var en langt reisen.|Ja, det vær et lang reise.;38|39;27.7|31.6;2.2|2.3;1
Jarl Torben;img/map_objects/Rensa/avatars/jarl_torben.png;Bot;Vil du legge deg?;41;11.6;1.2
Wiking;img/characters/thumbs/[[user.character_number]].png;Player;Ja, hvor er mitt seng?|Ja, hvor er mine seng?|Ja, hvor er min seng?;38|39;30.8|35.6;1.6|1.6;3']);

	    $fable2Id = DB::table('tasks')->insertGetId(['name'=>'Poznaj Rensę', 'type_id'=>$fableTaskTypeId,
		    'chapter_id'=>$Chapter5Id, 'is_forced'=>1, 'parent_task_id' => $task2Id, 'content' => 'Pokaż mapę
Po miłym przywitaniu się z Jarlem Runsy, czas poznać innych mieszkańców portu. Poćwicz z nimi układanie zdań, a na pewno zyskasz sobie ich przychylność.']);

	    $task3Id = DB::table('tasks')->insertGetId(['name'=>'Pytania z Arnorą','exp'=>10,'type_id'=>$sentencesTaskTypeId,
		    'chapter_id'=>$Chapter5Id, 'pin_id'=>$botArnora, 'parent_task_id' => $fable2Id,
		    'content' =>'<h2>Nazywam się Arnora</h2><p>Cieszę się, że mogę Cię poznać. Za tobą pewnie długa poróż, może potrenujesz układanie pytań, żeby chwilę odpocząć?</p>
Czy macie duży ogród?;Har|dere|en|stor|hage?|et|liten;Har|dere|en|stor|hage?
Zmywacie naczynia?;Vasker|dere|opp?|vi|hage;Vasker|dere|opp?
Możesz posprzątać w salonie?;Kan|du|rydde|i|stua?|gå|på;Kan|du|rydde|i|stua?
Kto chce poodkurzać?;Hvem|vil|støvsuge?|Når|Hvor;Hvem|vil|støvsuge?
Potrzebujesz pomocy?;Trenger|du|hjelp?|henne|de;Trenger|du|hjelp?']);

	    $task4Id = DB::table('tasks')->insertGetId(['name'=>'Zdania oznajmujące z Vestarem','exp'=>10,'type_id'=>$sentencesTaskTypeId,
		    'chapter_id'=>$Chapter5Id, 'pin_id'=>$botVestar, 'parent_task_id' => $task3Id,
		    'content' =>'<h2>O, Ciebie nie znam</h2><p>Ale cieszę się, że tu jesteś. Chcesz pomóc mi w zadaniu domowym? Muszę przetłumaczyć kilka zdań na norweski...</p>
Mamy trzy obrazy na ścianie.;Vi|har|tre|bilder|på|veggen.|to|ikke;Vi|har|tre|bilder|på|veggen.
Ręczniki są pod lustrem.;Håndkler|er|under|speilet.|blir|ikke;Håndkler|er|under|speilet.
Tom jest głodny i chce mu się pić.;Tom|er|sulten|og|tørst.|ikke|Heidi;Tom|er|sulten|og|tørst.
Jedzenie jest w kuchni.;Maten|er|på|kjøkkenet.|soverommmet.|i;Maten|er|på|kjøkkenet.
Odrabiam lekcje po obiedzie.;Jeg|gjør|lekser|etter|middag.|Du|før;Jeg|gjør|lekser|etter|middag.']);

	    $fable3Id = DB::table('tasks')->insertGetId(['name'=>'Zakończenie zadań z piątego rozdziału', 'type_id'=>$fableTaskTypeId,
		    'chapter_id'=>$Chapter5Id, 'is_forced'=>1, 'parent_task_id' => $task4Id, 'content' => 'Pokaż mapę
<p>Ciekawe co przyniosą kolejne dni w Runsie. Jeżeli kiedyś znajdziesz Miecz Lata (Sommerens Sverd), to pamiętaj, żeby zanieść go Sigve. Z pewnością sowicie Cię za niego wynagrodzi :)</p>
<p>Na teraz to koniec. Pamiętaj, że możesz poprawić swoje odpowiedzi w zadaniach układania słówek.</p>']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
