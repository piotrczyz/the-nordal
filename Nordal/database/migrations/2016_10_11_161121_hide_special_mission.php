<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HideSpecialMission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::update('update pins set map_id = NULL where name = ?', ['Jaskinia']); //hiding pin Jaskinia from Nordal map
	    DB::update('update tasks set is_forced = 0 where name = ?', ['Powstań i walcz!']);
	    DB::update('update tasks set chapter_id = NULL where 19 <= id AND id <= 23');
	    DB::update('update tasks set parent_task_id = 18 where id = 24');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
