<?php

use Illuminate\Database\Migrations\Migration;

class AddNewTaskTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $type = new \Nordal\Models\TaskType();
	    $type->Name = 'Fabuła';
	    $type->Param1 = 'fable';
	    $type->save();
	    
	    $type = new \Nordal\Models\TaskType();
	    $type->Name = 'Wybierz postać';
	    $type->Param1 = 'choose_character';
	    $type->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
