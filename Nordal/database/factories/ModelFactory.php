<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Nordal\Models\Pin;

$factory->define(Nordal\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\Nordal\Models\Task::class, function (Faker\Generator $faker) {
	return [
		
		'name' => $faker->name,
		'description' => $faker->text,
		'exp' => $faker->numerify('###'),
		'IsRepeatable' => $faker->boolean()
	];
});

$factory->define(\Nordal\Models\TaskType::class, function (Faker\Generator $faker) {
	return [

		'Name' => $faker->name,
		'Param1' => $faker->words(1)
	];
});

$factory->define(Pin::class, function(\Faker\Generator $faker){
	return [
			
	];
});